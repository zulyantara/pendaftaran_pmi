<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agama_model extends CI_Model
{
  var $table = 'agamas';
  var $field = array('id', 'agama', 'created_at', 'update_at');

  public function __construct()
  {
    parent::__construct();
  }

  public function get_data($where = '')
  {
    if ($where !== '')
    {
      if (is_array($where))
      {
        foreach ($where as $key => $value)
        {
          $this->db->where($key, $value);
        }
      }
      else
      {
        $this->db->where($where);
      }
    }

    $this->db->from($this->table);

    return $this->db->get();
  }
}
