<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_profile_model extends CI_Model
{
  var $table = 'user_profiles';

  public function __construct()
  {
    parent::__construct();
  }

  public function get_data($where = '')
  {
    if ($where !== '')
    {
      if (is_array($where))
      {
        foreach ($where as $key => $value)
        {
          $this->db->where($key, $value);
        }
      }
      else
      {
        $this->db->where($where);
      }
    }

    $this->db->from($this->table);

    return $this->db->get();
  }

  function store($data)
  {
    // echo "<pre>";var_dump($data);echo "</pre>";exit;
    $sub_jenis_anggota = $data['sub_jenis_anggota'];
    $profile = $data['profile'];
    $penghargaan = $data['penghargaan'];
    $penugasan = $data['penugasan'];
    $userunit = $data['userunit'];
    $pelatihan = $data['pelatihan'];

    $this->db->trans_start();

    $config['upload_path'] = './uploads/images/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = 2048;
    $config['max_width'] = 1300;
    $config['max_height'] = 1300;
    $config['file_name'] = underscore($profile['nama'])."_".date('Y-m-d');

    $this->load->library('upload', $config);

    //simpan profile
    if ($this->upload->do_upload('profilephoto'))
    {
      $dataProfile['image'] = base_url('uploads/images/'.$this->upload->data('file_name'));
    }
    $dataProfile['name'] = $profile['nama'];
    $dataProfile['tempat_lahir'] = $profile['tempat_lahir'];
    $dataProfile['tanggal_lahir'] = date('Y-m-d', strtotime($profile['tanggal_lahir']));
    $dataProfile['gender'] = $profile['gender'];
    $dataProfile['agama'] = $profile['agama'];
    $dataProfile['provinsi'] = $profile['provinsi'];
    $dataProfile['kabupaten'] = $profile['kabupaten'];
    $dataProfile['kecamatan'] = $profile['kecamatan'];
    $dataProfile['desa'] = $profile['desa'];
    $dataProfile['unit_id'] = $userunit['unit'];
    $dataProfile['sub_jenis_anggota_id'] = $sub_jenis_anggota;
    ($profile['spesialisasi'] !== '') ? $dataProfile['spesialisasi'] = $profile['spesialisasi'] : '';
    // $dataProfile['reward'] = $profile['reward'];
    // $dataProfile['penugasan'] = $profile['penugasan'];
    $dataProfile['email'] = $profile['email'];
    $dataProfile['idcard'] = $profile['idcard'];
    $dataProfile['no_telp'] = $profile['no_telp'];
    $dataProfile['gol_darah'] = $profile['gol_darah'];
    $dataProfile['created_at'] = date('Y-m-d H:i:s');
    $this->db->insert('user_profiles', $dataProfile);
    $userProfileId = $this->db->insert_id();

    // simpan user unit
    // $dataUserUnit['user_profile_id'] = $userProfileId;
    // $dataUserUnit['unit_id'] = $userunit['unit'];
    // $dataUserUnit['created_at'] = date('Y-m-d H:i:s');
    // $this->db->insert('user_units', $dataUserUnit);

    // simpan user penghargaan
    if (isset($penghargaan))
    {
      for ($i=0; $i < count($penghargaan); $i++)
      {
        $dataUserPenghargaan['user_profile_id'] = $userProfileId;
        $dataUserPenghargaan['penghargaan'] = $penghargaan[$i];
        $this->db->insert('user_penghargaans', $dataUserPenghargaan);
      }
    }

    // simpan user penugasan
    if (isset($penugasan))
    {
      for ($i=0; $i < count($penugasan); $i++)
      {
        $dataUserPenugasan['user_profile_id'] = $userProfileId;
        $dataUserPenugasan['penugasan'] = $penugasan[$i];
        $this->db->insert('user_penugasans', $dataUserPenugasan);
      }
    }

    // simpan user pelatihan
    if (isset($pelatihan))
    {
      for ($i=0; $i < count($pelatihan); $i++)
      {
        $dataUserPelatihan['user_profile_id'] = $userProfileId;
        $dataUserPelatihan['pelatihan_id'] = $pelatihan[$i];
        $this->db->insert('user_pelatihans', $dataUserPelatihan);
      }
    }

    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE)
    {
      return 'error';
    }
    else
    {
      return 'success';
    }
  }

  public function delete(Array $params = NULL)
  {
    if ( ! is_null($params)) {
      if (array_key_exists('where', $params)) {
        if (is_array($params['where'])) {
          foreach ($params['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($params['where']);
        }
      }
    }

    $this->db->delete($this->table);
  }
}
