<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_penghargaan_model extends CI_Model
{
  var $table = 'user_penghargaans';
  var $field = array('id', 'user_profile_id', 'jenis_pelatihan_id', 'created_at', 'update_at');

  public function __construct()
  {
    parent::__construct();
  }

  public function get_data($where = '')
  {
    if ($where !== '')
    {
      if (is_array($where))
      {
        foreach ($where as $key => $value)
        {
          $this->db->where($key, $value);
        }
      }
      else
      {
        $this->db->where($where);
      }
    }

    $this->db->from($this->table);

    return $this->db->get();
  }

  public function delete(Array $params = NULL)
  {
    if ( ! is_null($params)) {
      if (array_key_exists('where', $params)) {
        if (is_array($params['where'])) {
          foreach ($params['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($params['where']);
        }
      }
    }

    $this->db->delete($this->table);
  }
}
