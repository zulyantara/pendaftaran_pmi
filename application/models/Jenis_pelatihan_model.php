<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_pelatihan_model extends CI_Model
{
  var $table = 'jenis_pelatihans';
  var $field = array('id', 'jenis_pelatihan', 'created_at', 'update_at');

  public function __construct()
  {
    parent::__construct();
  }

  public function get_data($where = '')
  {
    if ($where !== '')
    {
      if (is_array($where))
      {
        foreach ($where as $key => $value)
        {
          $this->db->where($key, $value);
        }
      }
      else
      {
        $this->db->where($where);
      }
    }

    $this->db->from($this->table);

    return $this->db->get();
  }

  public function delete(Array $params = NULL)
  {
    if ( ! is_null($params)) {
      if (array_key_exists('where', $params)) {
        if (is_array($params['where'])) {
          foreach ($params['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($params['where']);
        }
      }
    }

    $this->db->delete($this->table);
  }
}
