<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class V_user_profile_model extends CI_Model
{
    var $table = 'v_user_profile';
    var $column_order = array('name');
    var $column_search = array('name');
    var $order = array('id' => 'desc');

    public function __construct()
    {
      parent::__construct();
    }

    private function _get_datatables_query($post)
    {
      if ($this->uri->segment(3) == 'jakarta_barat')
      {
        $this->db->where('kabupaten_unit_id', 1);
      }
      elseif ($this->uri->segment(3) == 'jakarta_pusat')
      {
        $this->db->where('kabupaten_unit_id', 2);
      }
      elseif ($this->uri->segment(3) == 'jakarta_selatan')
      {
        $this->db->where('kabupaten_unit_id', 3);
      }
      elseif ($this->uri->segment(3) == 'jakarta_timur')
      {
        $this->db->where('kabupaten_unit_id', 4);
      }
      elseif ($this->uri->segment(3) == 'jakarta_utara')
      {
        $this->db->where('kabupaten_unit_id', 5);
      }
      elseif ($this->uri->segment(3) == 'kep_seribu')
      {
        $this->db->where('kabupaten_unit_id', 6);
      }

      if($post['jenis_anggota'])
      {
          $this->db->where('jenis_anggota_id', $post['jenis_anggota']);
      }
      if($post['sub_jenis_anggota'])
      {
          $this->db->where('sub_jenis_anggota_id', $post['sub_jenis_anggota']);
      }
      if($post['kabupaten'])
      {
          $this->db->where('kabupaten_unit_id', $post['kabupaten']);
      }
      if($post['unit'])
      {
          $this->db->where('unit_id', $post['unit']);
      }

      $this->db->from($this->table);

      $i = 0;

      foreach ($this->column_search as $item) // loop column
      {
        if($post['search']['value']) // if datatable send POST for search
        {
          if($i===0) // first loop
          {
            $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
            $this->db->like($item, $post['search']['value']);
          }
          else
          {
            $this->db->or_like($item, $post['search']['value']);
          }

          if(count($this->column_search) - 1 == $i) //last loop
          {
            $this->db->group_end(); //close bracket
          }
        }
        $i++;
      }

      if(isset($post['order'])) // here order processing
      {
        $this->db->order_by($this->column_order[$post['order']['0']['column']], $post['order']['0']['dir']);
      }
      else if(isset($this->order))
      {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
      }
    }

    function get_datatables($post)
    {
      $this->_get_datatables_query($post);
      if($post['length'] != -1)
      {
          $this->db->limit($post['length'], $post['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_filtered($post)
    {
      $this->_get_datatables_query($post);
      $query = $this->db->get();
      return $query->num_rows();
    }

    public function count_all()
    {
      $this->db->from($this->table);
      return $this->db->count_all_results();
    }

    function get_data($where = FALSE)
    {
      if ($where !== FALSE)
      {
        foreach ($where as $key => $value)
        {
          $this->db->where($key, $value);
        }
      }
      // echo $this->db->get_compiled_select($this->table);exit;
      $this->db->from($this->table);

      return $this->db->get();
    }
}
