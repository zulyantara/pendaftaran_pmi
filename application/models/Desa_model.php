<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desa_model extends CI_Model
{
  var $table = 'desas';

  public function __construct()
  {
    parent::__construct();
  }

  public function get_data(Array $params = NULL)
  {
    if ( ! is_null($params)) {
      if (array_key_exists('where', $params)) {
        if (is_array($params['where']))
        {
          foreach ($params['where'] as $key => $value)
          {
            $this->db->where($key, $value);
          }
        }
        else
        {
          $this->db->where($params['where']);
        }
      }

      if (array_key_exists('order_by', $params)) {
        $this->db->order_by($params['order_by']);
      }

      if (array_key_exists('select_max', $params)) {
        $this->db->select_max($params['select_max']);
      }
    }

    $this->db->from($this->table);
    // echo $this->db->get_compiled_select();exit;
    return $this->db->get();
  }

  public function create($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->affected_rows();
  }

  public function update(Array $params = NULL)
  {
    if ( ! is_null($params)) {
      if (array_key_exists('where', $params)) {
        if (is_array($params['where'])) {
          foreach ($params['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($params['where']);
        }
      }

      if (array_key_exists('data', $params)) {
        $this->db->update($this->table, $params['data']);
        return $this->db->affected_rows();
      }
    }
  }

  public function delete(Array $params = NULL)
  {
    if ( ! is_null($params)) {
      if (array_key_exists('where', $params)) {
        if (is_array($params['where'])) {
          foreach ($params['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($params['where']);
        }
      }
    }

    $this->db->delete($this->table);
  }
}
