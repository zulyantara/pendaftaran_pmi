<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class V_user_pelatihan_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function get_data($where = '')
  {
    if ($where !== '')
    {
      foreach ($where as $key => $value)
      {
        $this->db->where($key, $value);
      }
    }

    $this->db->from('v_user_pelatihan');

    return $this->db->get();
  }
}
