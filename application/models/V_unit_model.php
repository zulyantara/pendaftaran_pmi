<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class V_unit_model extends CI_Model
{
  var $table = 'v_units';
  var $column_order = array(null, 'unit', 'kabupaten', 'jenis_anggota', 'sub_jenis_anggota');
  var $column_search = array('unit');
  var $order = array('id' => 'desc');

  public function __construct()
  {
    parent::__construct();
  }

  public function get_data(Array $params = NULL)
  {
    if ( ! is_null($params)) {
      if (array_key_exists('where', $params)) {
        if (is_array($params['where']))
        {
          foreach ($params['where'] as $key => $value)
          {
            $this->db->where($key, $value);
          }
        }
        else
        {
          $this->db->where($params['where']);
        }
      }

      if (array_key_exists('order_by', $params)) {
        $this->db->order_by($params['order_by']);
      }

      if (array_key_exists('select_max', $params)) {
        $this->db->select_max($params['select_max']);
      }
    }

    $this->db->from($this->table);
    // echo $this->db->get_compiled_select();exit;
    return $this->db->get();
  }

  private function _get_datatables_query($post)
  {
    if($post['kabupaten'])
    {
        $this->db->where('kabupaten_id', $post['kabupaten']);
    }
    if($post['jenis_anggota'])
    {
        $this->db->where('jenis_anggota_id', $post['jenis_anggota']);
    }
    if($post['sub_jenis_anggota'])
    {
      $this->db->where('sub_jenis_anggota_id', $post['sub_jenis_anggota']);
    }
    $this->db->from($this->table);

    $i = 0;

    foreach ($this->column_search as $item) // loop column
    {
      if($post['search']['value']) // if datatable send POST for search
      {
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $post['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $post['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
        {
          $this->db->group_end(); //close bracket
        }
      }
      $i++;
    }

    if(isset($post['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$post['order']['0']['column']], $post['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables($post)
  {
    $this->_get_datatables_query($post);
    if($post['length'] != -1)
    {
        $this->db->limit($post['length'], $post['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered($post)
  {
    $this->_get_datatables_query($post);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
}
