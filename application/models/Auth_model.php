<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    /*
     * @author zulyantara <zulyantara@gmail.com>
     * @copyright copyright 2016 zulyantara
     */

    function __construct()
    {
        parent::__construct();
    }

    function get_ha($select, $where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $val)
            {
                $this->db->where($key, $val);
            }
        }

        $this->db->select($select);
        $this->db->join('hak_akses ha', 'ha.ha_menu = m.menu_id', 'left');
        // echo $this->db->get_compiled_select('menu m');exit;
        return $this->db->get('menu m');
    }

    function validate($email)
    {
        // $hashAndSalt = $this->get_password($username);
        // $password_hash = password_hash($userpassword, PASSWORD_BCRYPT, array("cost" => 12));

        $this->db->select("id, name, email, password, active");
        $this->db->where('email', $email);
        $this->db->where('active', 1);
        return $query = $this->db->get('users');
        // echo $this->db->last_query();exit;
    }

    function check_password($pwd)
    {
        $this->db->where('user_id', $this->session->userdata('userId'));
        $this->db->where("user_active", 1);
        $query = $this->db->get('mt_user');
        // echo $this->db->last_query();exit;
        $row_user = $query->num_rows() > 0 ? $query->row() : FALSE;
        if ($row_user !== FALSE)
        {
            if (password_verify($pwd, $row_user->user_password))
            {
                // return $query->row();
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

    function update_password($data, $where = NULL)
    {
        if ($where !== NULL)
        {
            foreach ($where as $key => $value)
            {
                $this->db->where($key, $value);
            }
        }
        $this->db->update("mt_user", $data);
    }
}
