<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Jenis_anggota_model extends CI_Model
{
  var $table = 'jenis_anggotas';
  var $column_order = array(null, 'kode', 'jenis_anggota');
  var $column_search = array('kode', 'jenis_anggota');
  var $order = array('id' => 'desc');

  public function __construct()
  {
    parent::__construct();
  }

  public function get_data(Array $params = NULL)
  {
    if ( ! is_null($params)) {
      if (array_key_exists('where', $params)) {
        if (is_array($params['where']))
        {
          foreach ($params['where'] as $key => $value)
          {
            $this->db->where($key, $value);
          }
        }
        else
        {
          $this->db->where($params['where']);
        }
      }

      if (array_key_exists('order_by', $params)) {
        $this->db->order_by($params['order_by']);
      }

      if (array_key_exists('select_max', $params)) {
        $this->db->select_max($params['select_max']);
      }
    }

    $this->db->from($this->table);
    // echo $this->db->get_compiled_select();exit;
    return $this->db->get();
  }

  public function create($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->affected_rows();
  }

  public function update(Array $params = NULL)
  {
    if ( ! is_null($params)) {
      if (array_key_exists('where', $params)) {
        if (is_array($params['where'])) {
          foreach ($params['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($params['where']);
        }
      }
      
      if (array_key_exists('data', $params)) {
        $this->db->update($this->table, $params['data']);
        return $this->db->affected_rows();
      }
    }
  }

  public function delete(Array $params = NULL)
  {
    if ( ! is_null($params)) {
      if (array_key_exists('where', $params)) {
        if (is_array($params['where'])) {
          foreach ($params['where'] as $key => $value) {
            $this->db->where($key, $value);
          }
        }
        else {
          $this->db->where($params['where']);
        }
      }
    }

    $this->db->delete($this->table);
  }

  private function _get_datatables_query($post)
  {
    $this->db->from($this->table);

    $i = 0;

    foreach ($this->column_search as $item) // loop column
    {
      if($post['search']['value']) // if datatable send POST for search
      {
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $post['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $post['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) //last loop
        {
          $this->db->group_end(); //close bracket
        }
      }
      $i++;
    }

    if(isset($post['order'])) // here order processing
    {
      $this->db->order_by($this->column_order[$post['order']['0']['column']], $post['order']['0']['dir']);
    }
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  function get_datatables($post)
  {
    $this->_get_datatables_query($post);
    if($post['length'] != -1)
    {
        $this->db->limit($post['length'], $post['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered($post)
  {
    $this->_get_datatables_query($post);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
}
