<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Dashboard_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  function count_total_by_jenis_anggota($where = '')
  {
    if ($where !== '')
    {
      foreach ($where as $key => $value)
      {
        $this->db->where($key, $value);
      }
    }
    $this->db->from('v_total_by_jenis_anggota');
    return $this->db->get();
  }

  function count_total_by_kabupaten($where = '')
  {
    if ($where !== '')
    {
      foreach ($where as $key => $value)
      {
        $this->db->where($key, $value);
      }
    }
    $this->db->from('v_total_by_kabupaten');
    return $this->db->get();
  }
}
