<img src="<?php echo base_url('src/img/kop_surat.png'); ?>" alt=""><hr>
<table id="table_<?php echo $modul; ?>" class="table table-striped table-bordered table-hover" cellspacing="0" cellpadding="3" width="100%" border="0" style="font-size: 10px;">
  <thead>
    <tr>
      <th class="text-center">No</th>
      <th style="text-align: left;" width="150px">Nama</th>
      <th class="text-center">Jenis Kelamin</th>
      <th class="text-center">Jenis Anggota</th>
      <th class="text-center">Sub Jenis Anggota</th>
      <th class="text-center">Kabupaten/Kota</th>
      <th class="text-center">Unit</th>
    </tr>
  </thead>

  <tbody>
    <?php
    $no = 0;
    foreach ($rwUserProfile as $vUserProfile) {
      ?>
      <tr>
        <td><?php echo $no = $no + 1; ?></td>
        <td><?php echo $vUserProfile->name; ?></td>
        <td><?php echo $vUserProfile->gender; ?></td>
        <td><?php echo $vUserProfile->jenis_anggota; ?></td>
        <td align="center"><?php echo $vUserProfile->sub_jenis_anggota; ?></td>
        <td><?php echo $vUserProfile->kabupaten; ?></td>
        <td><?php echo $vUserProfile->unit; ?></td>
      </tr>
      <?php
    }
    ?>
  </tbody>
</table>
