<div class="box box-danger box-solid">
  <div class="box-header with-border">
    <h3 class="box-title">Cetak Relawan</h3>

    <div class="box-tools pull-right">
      <!-- <a href="<?php echo site_url($modul.'/show'); ?>" class="btn btn-red btn-sm btn-flat">Tambah Data</a> -->
      <button type="button" title="Refresh" onclick="reload_table();" class="btn btn-red btn-sm btn-flat">Refresh</button>
    </div>
  </div>
  <div class="box-body">
    <?php
    if ( ! is_null($this->session->flashdata('alert_class')))
    {
      ?>
      <div id="alert" class="alert alert-<?php echo $this->session->flashdata('alert_class'); ?>" role="alert"><?php echo $this->session->flashdata('alert_ket'); ?></div>
      <?php
    }
    ?>

    <div class="row">
      <div class="col-md-12">
        <div class="box box-default box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Advance Search</h3>
          </div>

          <div class="box-body">
            <form class="form-inline" id="frm_search">
              <div class="form-group">
                <label class="sr-only" for="opt_jenis_anggota">Jenis Anggota</label>
                <select id="opt_jenis_anggota" class="form-control">
                  <option value="">Pilih Jenis Anggota</option>
                  <?php
                  foreach ($rJenisAnggota as $vJenisAnggota) {
                    ?>
                    <option value="<?php cetak($vJenisAnggota->id); ?>"><?php cetak($vJenisAnggota->jenis_anggota); ?></option>
                    <?php
                  }
                  ?>
                </select>
              </div>
              <div class="form-group">
                <label class="sr-only" for="opt_sub_jenis_anggota">Sub Jenis Anggota</label>
                <select id="opt_sub_jenis_anggota" class="form-control">
                  <option value="">Pilih Sub Jenis Anggota</option>
                </select>
              </div>
              <div class="form-group">
                <label class="sr-only" for="opt_kabupaten">Kabupaten</label>
                <select id="opt_kabupaten" class="form-control">
                  <option value="">Pilih Kabupaten</option>
                  <?php
                  foreach ($rKabupaten as $vKabupaten) {
                    ?>
                    <option value="<?php cetak($vKabupaten->id); ?>"><?php cetak($vKabupaten->kabupaten); ?></option>
                    <?php
                  }
                  ?>
                </select>
              </div>
              <div class="form-group">
                <label class="sr-only" for="opt_unit">Unit</label>
                <select id="opt_unit" class="form-control">
                  <option value="">Pilih Unit</option>
                </select>
              </div>
              <button type="button" id="search" class="btn btn-success">Search</button>
              <button type="button" id="btn_reset" class="btn btn-danger">Reset</button>
              <button type="button" id="cetak" class="btn btn-primary">Cetak</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div id="myTable">
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $("#alert").fadeTo(5000, 500).slideUp(500, function(){
    $("#alert").slideUp(500);
  });

  $('#search').click(function(){
    var jenis_anggota_id = $('#opt_jenis_anggota').val();
    var sub_jenis_anggota_id = $('#opt_sub_jenis_anggota').val();
    var kabupaten_id = $('#opt_kabupaten').val();
    var unit_id = $('#opt_unit').val();

    $.ajax({
      url: '<?php echo site_url($modul.'/show'); ?>',
      data: {
        id_jenis_anggota: jenis_anggota_id,
        id_sub_jenis_anggota: sub_jenis_anggota_id,
        id_kabupaten: kabupaten_id,
        id_unit: unit_id,
        cetak: 'tidak'
      },
      method: 'POST',
      success: function(response) {
        $('#myTable').html(response);
      }
    });
  });

  $('#btn_reset').click(function(){
    $('#frm_search')[0].reset();
    $('#myTable').html('');
  });

  $('#cetak').click(function(){
    var jenis_anggota_id = $('#opt_jenis_anggota').val();
    var sub_jenis_anggota_id = $('#opt_sub_jenis_anggota').val();
    var kabupaten_id = $('#opt_kabupaten').val();
    var unit_id = $('#opt_unit').val();

    window.open('<?php echo site_url($modul.'/cetak/'); ?>'+jenis_anggota_id+'/'+sub_jenis_anggota_id+'/'+kabupaten_id+'/'+unit_id, '_blank');

    // $.ajax({
    //   url: '<?php echo site_url($modul.'/cetak'); ?>',
    //   data: {
    //     id_jenis_anggota: jenis_anggota_id,
    //     id_sub_jenis_anggota: sub_jenis_anggota_id,
    //     id_kabupaten: kabupaten_id,
    //     id_unit: unit_id,
    //     cetak: 'ya'
    //   },
    //   method: 'POST',
    //   success: function(response) {
    //     window.open()
    //   }
    // });
  });

  $('#opt_jenis_anggota').change(function(){
    var jenis_anggota_id = $(this).val();
    $.ajax({
      url: '<?php echo site_url('Sub_jenis_anggota/ajax_by_jenis_anggota_id'); ?>',
      data: {jenis_anggota_id:jenis_anggota_id},
      dataType: 'json',
      type: 'POST',
      success: function(responce) {
        $('#opt_sub_jenis_anggota').empty();
        $('#opt_sub_jenis_anggota').append('<option value="">Pilih Sub Jenis Anggota</option>');
        $.each(responce, function(key, value) {
          $('#opt_sub_jenis_anggota').append('<option value="'+value.id+'">'+value.sub_jenis_anggota+'</option>');
        });
      }
    });
  });

  $('#opt_kabupaten').change(function(){
    var kabupaten_id = $(this).val();
    var sub_jenis_anggota_id = $('#opt_sub_jenis_anggota').val();

    // if (sub_jenis_anggota_id == '') {
    //   alert('Pilih Sub Jenis Anggota terlebih dahulu!');
    // }

    $.ajax({
      url: '<?php echo site_url('Unit/ajax_by_kabupaten_id'); ?>',
      data: {
        kabupaten_id:kabupaten_id,
        sub_jenis_anggota_id:sub_jenis_anggota_id
      },
      dataType: 'json',
      type: 'POST',
      success: function(responce) {
        $('#opt_unit').empty();
        $('#opt_unit').append('<option value="">Pilih Unit</option>');
        $.each(responce, function(key, value) {
          $('#opt_unit').append('<option value="'+value.id+'">'+value.unit+'</option>');
        });
      }
    });
  });
});
</script>
