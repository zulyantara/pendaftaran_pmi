<?php
$id = isset($row) ? $row->id : '';
$sub_jenis_anggota = isset($row) ? $row->sub_jenis_anggota : '';
$jenis_anggota = isset($row) ? $row->jenis_anggota_id : '';
$val_btn = isset($row) ? 'update' : 'save';
?>

<div class="box box-danger box-solid">
  <div class="box-header with-border">
    <h3 class="box-title">Jenis Anggota</h3>

    <div class="box-tools pull-right">
      <a href="<?php echo site_url($modul); ?>" class="btn btn-red btn-sm btn-flat">List</a>
    </div>
  </div>
  <div class="box-body">
    <form action="<?php echo site_url($modul.'/submit'); ?>" method="post" class="form-horizontal">
      <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
      <div class="form-group">
        <label for="sub_jenis_anggota" class="col-sm-2 control-label">Sub Jenis Anggota</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="sub_jenis_anggota" id="sub_jenis_anggota" placeholder="Sub Jenis Anggota" autofocus="autofocus" value="<?php cetak($sub_jenis_anggota); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="jenis_anggota" class="col-sm-2 control-label">Jenis Anggota</label>
        <div class="col-sm-10">
          <select name="jenis_anggota" id="jenis_anggota" class="form-control">
            <option value="">Pilih Jenis Anggota</option>
            <?php
            foreach ($rwJenisAnggota as $valJenisAnggota) {
              $selJenisAnggota = $valJenisAnggota->id == $jenis_anggota ? 'selected="selected"' : '';
              ?>
              <option value="<?php cetak($valJenisAnggota->id); ?>" <?php echo $selJenisAnggota; ?>><?php cetak($valJenisAnggota->jenis_anggota); ?></option>
              <?php
            }
            ?>
          </select>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="submit" id="submit" value="<?php echo $val_btn; ?>" class="btn btn-primary btn-flat">Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>
