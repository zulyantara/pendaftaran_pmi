<div class="box box-danger box-solid">
  <div class="box-header with-border">
    <h3 class="box-title">Jenis Anggota</h3>

    <div class="box-tools pull-right">
      <a href="<?php echo site_url($modul.'/show'); ?>" class="btn btn-red btn-sm btn-flat">Tambah Data</a>
      <button type="button" title="Refresh" onclick="reload_table();" class="btn btn-red btn-sm btn-flat">Refresh</button>
    </div>
  </div>
  <div class="box-body">
    <?php
    if ( ! is_null($this->session->flashdata('alert_class')))
    {
      ?>
      <div id="alert" class="alert alert-<?php echo $this->session->flashdata('alert_class'); ?>" role="alert"><?php echo $this->session->flashdata('alert_ket'); ?></div>
      <?php
    }
    ?>

    <table id="table_<?php echo $modul; ?>" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th class="text-center">No</th>
          <th class="text-center">Kode</th>
          <th class="text-center">Jenis Anggota</th>
          <th class="text-center" style="width:150px;">Action</th>
        </tr>
      </thead>

      <tbody>
      </tbody>
    </table>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $("#alert").fadeTo(5000, 500).slideUp(500, function(){
    $("#alert").slideUp(500);
  });

  //datatables
  var table = $('#table_<?php echo $modul; ?>').DataTable({

    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": "<?php echo site_url($modul.'/data_grid')?>",
      "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      {
        "targets": [ 0 ], //first column
        "orderable": false, //set not orderable
      },
      {
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },

    ],

  });

  //check all
  $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });

  $("#success-alert").fadeTo(5000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
  });
});

function deleteItem(i, a)
{
  if (confirm("Anda yakin menghapus data "+a+"?"))
  {
    $.ajax({
      url: '<?php echo site_url($modul.'/destroy'); ?>',
      method: 'POST',
      data: {id: i},
      success: function(response){
        alert(response);
        $('#table_<?php echo $modul; ?>').DataTable().ajax.reload();
      },
    });
  }
  return false;
}

function reload_table()
{
  $('#table_<?php echo $modul; ?>').DataTable().ajax.reload();
}
</script>