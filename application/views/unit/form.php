<?php
$id = isset($row) ? $row->id : '';
$unit = isset($row) ? $row->unit : '';
$kabupaten = isset($row) ? $row->kabupaten_id : '';
$jenis_anggota = isset($row) ? $row->jenis_anggota_id : '';
$sub_jenis_anggota = isset($row) ? $row->sub_jenis_anggota_id : '';
$val_btn = isset($row) ? 'update' : 'save';
?>

<div class="box box-danger box-solid">
  <div class="box-header with-border">
    <h3 class="box-title">Unit</h3>

    <div class="box-tools pull-right">
      <a href="<?php echo site_url($modul); ?>" class="btn btn-red btn-sm btn-flat">List</a>
    </div>
  </div>
  <div class="box-body">
    <form action="<?php echo site_url($modul.'/submit'); ?>" method="post" class="form-horizontal">
      <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
      <div class="form-group">
        <label for="unit" class="col-sm-2 control-label">Unit</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="unit" id="unit" placeholder="Unit" autofocus="autofocus" value="<?php echo $unit; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="kabupaten" class="col-sm-2 control-label">Kabupaten/Kota</label>
        <div class="col-sm-10">
          <select class="form-control" name="kabupaten" id="kabupaten">
            <option value="">Pilih Kabupaten/Kota</option>
            <?php
            foreach ($rwKabupaten as $valKabupaten) {
              $selKabupaten = $valKabupaten->id == $kabupaten ? 'selected="selected"' : '';
              ?>
              <option value="<?php echo $valKabupaten->id; ?>" <?php echo $selKabupaten; ?>><?php echo $valKabupaten->kabupaten; ?></option>
              <?php
            }
            ?>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="jenis_anggota" class="col-sm-2 control-label">Jenis Anggota</label>
        <div class="col-sm-10">
          <select class="form-control" name="jenis_anggota" id="jenis_anggota">
            <option value="">Pilih Jenis Anggota</option>
            <?php
            foreach ($rwJenisAnggota as $valJenisAnggota) {
              $selJenisAnggota = $valJenisAnggota->id == $jenis_anggota ? 'selected="selected"' : '';
              ?>
              <option value="<?php echo $valJenisAnggota->id; ?>" <?php echo $selJenisAnggota; ?>><?php echo $valJenisAnggota->jenis_anggota; ?></option>
              <?php
            }
            ?>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="sub_jenis_anggota" class="col-sm-2 control-label">Sub Jenis Anggota</label>
        <div class="col-sm-10">
          <select class="form-control" name="sub_jenis_anggota" id="sub_jenis_anggota">
            <option value="">Pilih Sub Jenis Anggota</option>
            <?php
            if (isset($row)) {
              foreach ($rwSubJenisAnggota as $valSubJenisAnggota) {
                $selSubJenisAnggota = $valSubJenisAnggota->id == $sub_jenis_anggota ? 'selected="selected"' : '';
                ?>
                <option value="<?php echo $valSubJenisAnggota->id; ?>" <?php echo $selSubJenisAnggota; ?>><?php echo $valSubJenisAnggota->sub_jenis_anggota; ?></option>
                <?php
              }
            }
            ?>
          </select>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="submit" id="submit" value="<?php echo $val_btn; ?>" class="btn btn-primary btn-flat">Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $('#jenis_anggota').change(function(){
    var jenis_anggota_id = $('#jenis_anggota').val();
    $.ajax({
      url: '<?php echo site_url('Sub_jenis_anggota/ajax_by_jenis_anggota_id'); ?>',
      data: {jenis_anggota_id:jenis_anggota_id},
      dataType: 'json',
      type: 'POST',
      success: function(responce) {
        $('#sub_jenis_anggota').empty();
        $('#sub_jenis_anggota').append('<option value="">Pilih Sub Jenis Anggota</option>');
        $.each(responce, function(key, value) {
          $('#sub_jenis_anggota').append('<option value="'+value.id+'">'+value.sub_jenis_anggota+'</option>');
        });
      }
    });
  });
})
</script>
