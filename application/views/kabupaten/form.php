<?php
$id = isset($row) ? $row->id : '';
$kabupaten = isset($row) ? $row->kabupaten : '';
$provinsi = isset($row) ? $row->provinsi_id : '';
$val_btn = isset($row) ? 'update' : 'save';
?>

<div class="box box-danger box-solid">
  <div class="box-header with-border">
    <h3 class="box-title">Provinsi</h3>

    <div class="box-tools pull-right">
      <a href="<?php echo site_url($modul); ?>" class="btn btn-red btn-sm btn-flat">List</a>
    </div>
  </div>
  <div class="box-body">
    <form action="<?php echo site_url($modul.'/submit'); ?>" method="post" class="form-horizontal">
      <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
      <div class="form-group">
        <label for="kabupaten" class="col-sm-2 control-label">Kabupaten</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="kabupaten" id="kabupaten" placeholder="Kabupaten" autofocus="autofocus" value="<?php cetak($kabupaten); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="provinsi" class="col-sm-2 control-label">Provinsi</label>
        <div class="col-sm-10">
          <select name="provinsi" id="provinsi" class="form-control">
            <option value="">Pilih Provinsi</option>
            <?php
            foreach ($rwProvinsi as $valProvinsi) {
              $selProvinsi = $valProvinsi->id == $provinsi ? 'selected="selected"' : '';
              ?>
              <option value="<?php cetak($valProvinsi->id); ?>" <?php echo $selProvinsi; ?>><?php cetak($valProvinsi->provinsi); ?></option>
              <?php
            }
            ?>
          </select>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="submit" id="submit" value="<?php echo $val_btn; ?>" class="btn btn-primary btn-flat">Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>
