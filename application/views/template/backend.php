<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dahsboard PMI DKI Jakarta</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/Ionicons/css/ionicons.min.css'); ?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('vendor/almasaeed2010/adminlte/dist/css/AdminLTE.min.css'); ?>">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="<?php echo base_url('vendor/almasaeed2010/adminlte/dist/css/skins/skin-blue.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('src/css/my_style.css'); ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <script src="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('vendor/almasaeed2010/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('vendor/almasaeed2010/adminlte/dist/js/adminlte.min.js'); ?>"></script>
  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">
      <div class="row text-center">
        <img src="<?php echo base_url('src/img/identityTop.png'); ?>" alt="">
      </div>

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo site_url('Dashboard'); ?>" class="navbar-brand"><img src="<?php echo base_url('src/img/logopmijakarta.png'); ?>"></a>
        <!-- <a href="index2.html" class="logo">
          <span class="logo-mini"><b>PMI</b></span>
          <span class="logo-lg">PMI DKI Jakarta</span>
        </a> -->

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <!-- <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a> -->
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
          </div>

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Main Menu</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="<?php echo site_url('Dashboard'); ?>"><i class="fa fa-circle-o"></i> <span>Dashboard</span></a></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-circle-o"></i> <span>Kabupaten/Kota</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo site_url('Dashboard/jakarta_pusat'); ?>"><i class="fa fa-circle"></i> <span>Jakarta Pusat</span></a></li>
                <li><a href="<?php echo site_url('Dashboard/jakarta_utara'); ?>"><i class="fa fa-circle"></i> <span>Jakarta Utara</span></a></li>
                <li><a href="<?php echo site_url('Dashboard/jakarta_barat'); ?>"><i class="fa fa-circle"></i> <span>Jakarta Barat</span></a></li>
                <li><a href="<?php echo site_url('Dashboard/jakarta_selatan'); ?>"><i class="fa fa-circle"></i> <span>Jakarta Selatan</span></a></li>
                <li><a href="<?php echo site_url('Dashboard/jakarta_timur'); ?>"><i class="fa fa-circle"></i> <span>Jakarta Timur</span></a></li>
                <li><a href="<?php echo site_url('Dashboard/kep_seribu'); ?>"><i class="fa fa-circle"></i> <span>Kep. Seribu</span></a></li>
              </ul>
            </li>
            <li><a href="<?php echo site_url(''); ?>" target="_blank"><i class="fa fa-circle-o"></i> Pendaftaran</a></li>
            <li><a href="<?php echo site_url('Relawan'); ?>"><i class="fa fa-circle-o"></i> Relawan</a></li>
            <li><a href="<?php echo site_url('Cetak_relawan'); ?>"><i class="fa fa-circle-o"></i> Cetak Relawan</a></li>
            <?php
            if ($this->session->userdata('isLoggedIn') === TRUE) {
              ?>
              <li><a href="<?php echo site_url('Auth/logout'); ?>"><i class="fa fa-circle-o"></i> Logout</a></li>
              <?php
            }
            else {
              ?>
              <li><a href="<?php echo site_url('Auth'); ?>"><i class="fa fa-circle-o"></i> Login</a></li>
              <?php
            }
            ?>

            <?php
            if ($this->session->userdata('isLoggedIn') == TRUE) {
              ?>
              <li class="header">Master</li>
              <!-- Optionally, you can add icons to the links -->
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-circle-o"></i> <span>Anggota</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?php echo site_url('Jenis_anggota'); ?>"><i class="fa fa-circle"></i> <span>Jenis Anggota</span></a></li>
                  <li><a href="<?php echo site_url('Sub_jenis_anggota'); ?>"><i class="fa fa-circle"></i> <span>Sub Jenis Anggota</span></a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-circle-o"></i> <span>Wilayah</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?php echo site_url('Kabupaten'); ?>"><i class="fa fa-circle"></i> <span>Kabupaten/Kota</span></a></li>
                  <li><a href="<?php echo site_url('Kecamatan'); ?>"><i class="fa fa-circle"></i> <span>Kecamatan</span></a></li>
                  <li><a href="<?php echo site_url('Desa'); ?>"><i class="fa fa-circle"></i> <span>Kelurahan/Desa</span></a></li>
                </ul>
              </li>
              <li><a href="<?php echo site_url('Unit'); ?>"><i class="fa fa-circle-o"></i> <span>Unit</span></a></li>
              <?php
            }
            ?>
          </ul>
          <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Database Relawan PMI Provinsi DKI Jakarta
            <small><?php echo isset($kota) ? $kota : ''; ?></small>
          </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

          <!--------------------------
            | Your Page Content Here |
            -------------------------->
          <?php echo $contents; ?>

        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          <!-- Anything you want -->
        </div>
        <!-- Default to the left -->
        <strong><a href="http://pmidkijakarta.or.id">PMI DKI Jakarta</a>.</strong>
      </footer>

    </div>
    <!-- ./wrapper -->
  </body>
</html>
