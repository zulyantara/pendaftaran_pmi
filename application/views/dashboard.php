<div class="row">
  <div class="col-lg-3 col-xs-6">
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3><?php echo number_format($total_pmr,0,',','.'); ?></h3>

        <p>Palang Merah Remaja</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      <a href="#" id="btn-pmr" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>

  <div class="col-lg-3 col-xs-6">
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><?php echo number_format($total_ksr,0,',','.'); ?></h3>

        <p>Korps Sukarela</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      <a href="#" id="btn-ksr" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>

  <div class="col-lg-3 col-xs-6">
    <div class="small-box bg-red">
      <div class="inner">
        <h3><?php echo number_format($total_tsr,0,',','.'); ?></h3>

        <p>Tenaga Sukarela</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      <a href="#" id="btn-tsr" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>

  <div class="col-lg-3 col-xs-6">
    <div class="small-box bg-green">
      <div class="inner">
        <h3><?php echo number_format($total_anggota,0,',','.'); ?></h3>

        <p>Anggota</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      <a href="#" id="btn-anggota" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>

  <div class="col-md-12" id="pmr">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Detail PMR</h3>
      </div>

      <div class="box-body">
        <div class="row">
          <div class="col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Mula</span>
                <span class="info-box-number"><?php echo number_format($total_mula,0,',','.'); ?></span>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Madya</span>
                <span class="info-box-number"><?php echo number_format($total_madya,0,',','.'); ?></span>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Wira</span>
                <span class="info-box-number"><?php echo number_format($total_wira,0,',','.'); ?></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12" id="ksr">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Detail KSR</h3>
      </div>

      <div class="box-body">
        <div class="row">
          <div class="col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Laki-Laki</span>
                <span class="info-box-number"><?php echo number_format($total_ksr_m,0,',','.'); ?></span>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Perempuan</span>
                <span class="info-box-number"><?php echo number_format($total_ksr_f,0,',','.'); ?></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12" id="tsr">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Detail TSR</h3>
      </div>

      <div class="box-body">
        <div class="row">
          <div class="col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Laki-Laki</span>
                <span class="info-box-number"><?php echo number_format($total_tsr_m,0,',','.'); ?></span>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Perempuan</span>
                <span class="info-box-number"><?php echo number_format($total_tsr_f,0,',','.'); ?></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12" id="anggota">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Detail Anggota</h3>
      </div>

      <div class="box-body">
        <div class="row">
          <div class="col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Laki-Laki</span>
                <span class="info-box-number"><?php echo number_format($total_anggota_m,0,',','.'); ?></span>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Perempuan</span>
                <span class="info-box-number"><?php echo number_format($total_anggota_f,0,',','.'); ?></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">User Profiles</h3>
      </div>

      <div class="box-body">
        <table id="tbl-list" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Gender</th>
              <th>Created At</th>
              <th>Updated At</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $('#pmr').hide();
  $('#ksr').hide();
  $('#tsr').hide();
  $('#anggota').hide();

  var table = $('#tbl-list').DataTable({

    // "processing": true, //Feature control the processing indicator.
    // "serverSide": true, //Feature control DataTables' server-side processing mode.
    // "order": [], //Initial no order.
    //
    // // Load data for the table's content from an Ajax source
    // "ajax": {
    //   "url": "<?php echo site_url('Dashboard/dataGrid/'.$this->uri->segment(2))?>",
    //   "type": "POST"
    // },
    //
    // //Set column definition initialisation properties.
    // "columnDefs": [
    //   {
    //     "targets": [ -1 ], //last column
    //     "orderable": false, //set not orderable
    //   },
    //
    // ],

  });

  $('#btn-pmr').click(function(){
    $('#pmr').toggle('slow');
    $('#ksr').hide('slow');
    $('#tsr').hide('slow');
    $('#anggota').hide('slow');
  });

  $('#btn-ksr').click(function(){
    $('#ksr').toggle('slow');
    $('#pmr').hide('slow');
    $('#tsr').hide('slow');
    $('#anggota').hide('slow');
  });

  $('#btn-tsr').click(function(){
    $('#tsr').toggle('slow');
    $('#pmr').hide('slow');
    $('#ksr').hide('slow');
    $('#anggota').hide('slow');
  });

  $('#btn-anggota').click(function(){
    $('#anggota').toggle('slow');
    $('#pmr').hide('slow');
    $('#ksr').hide('slow');
    $('#tsr').hide('slow');
  });
});
</script>
