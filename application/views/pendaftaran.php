<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="box box-danger">

	<div class="box-header with-border">
		<h3 class="box-title">Form Register</h3>
	</div>

	<div class="box-body">
		<?php
		if ($this->session->flashdata('class') !== NULL)
		{
			?>
			<div class="alert alert-<?php echo $this->session->flashdata('class'); ?>" id="my-alert">
				<?php echo $this->session->flashdata('msg'); ?>
			</div>
			<?php
		}
		?>

		<form class="" action="<?php echo site_url('Pendaftaran'); ?>" method="post" enctype="multipart/form-data">
			<input type="hidden" name="status_pendaftaran" value="">
			<div class="row">

        <div class="col-md-4">
					<div class="form-group">
            <select class="form-control" id="jenis_anggota" name="jenis_anggota" required>
              <option value="">Pilih Jenis Anggota</option>
              <?php
							if (isset($resJenisAnggota))
							{
								if ($resJenisAnggota->num_rows() > 0)
								{
									foreach ($resJenisAnggota->result() as $valJenisAnggota)
									{
										?>
										<option value="<?php cetak($valJenisAnggota->id); ?>"><?php cetak($valJenisAnggota->jenis_anggota); ?></option>
										<?php
									}
								}
							}
              ?>
            </select>
          </div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
            <select class="form-control" id="sub_jenis_anggota" name="sub_jenis_anggota">
              <option value="">Pilih Sub Jenis Anggota</option>
            </select>
          </div>
        </div>

        <div class="col-md-12">
					<div class="box box-success">

						<div class="box-header with-border">
							<h3 class="box-title">Profile</h3>
							<div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
						</div>

						<div class="box-body">


		          <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="nama">Nama</label>
										<input type="text" class="form-control" id="nama" name="profile[nama]" placeholder="Nama" required>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="idcard">No KTA (Jika Ada)</label>
										<input type="text" class="form-control" id="idcard" name="profile[idcard]" placeholder="ID Card">
									</div>
								</div>

		            <div class="col-md-6">
		              <div class="form-group">
		                <label for="tempat_lahir">Tempat Lahir</label>
		                <input type="text" class="form-control" id="tempat_lahir" name="profile[tempat_lahir]" placeholder="Tempat Lahir" required>
		              </div>
		            </div>
		            <div class="col-md-6">
		              <div class="form-group">
		                <label for="tanggal_lahir">Tanggal Lahir</label>
		                <input type="text" class="form-control" id="tanggal_lahir" name="profile[tanggal_lahir]" placeholder="Tanggal Lahir" required>
		              </div>
		            </div>
		          </div>

		          <div class="row">
		            <div class="col-md-6">
		              <div class="form-group">
		                <label for="jenis_kelamin">Jenis Kelamin</label>
		                <select class="form-control" id="jenis_kelamin" name="profile[gender]" required>
		                  <option value="">Jenis Kelamin</option>
		                  <option value="m">Laki-Laki</option>
		                  <option value="f">Perempuan</option>
		                </select>
		              </div>
		            </div>
		            <div class="col-md-6">
		              <div class="form-group">
		                <label for="agama">Agama</label>
		                <select class="form-control" id="agama" name="profile[agama]" required>
		                  <option value="">Agama</option>
		                  <?php
											if (isset($resAgama))
											{
												if ($resAgama->num_rows() > 0)
												{
													foreach ($resAgama->result() as $vAgama)
													{
														?>
														<option value="<?php cetak($vAgama->id); ?>"><?php cetak($vAgama->agama); ?></option>
														<?php
													}
												}
											}
		                  ?>
		                </select>
		              </div>
		            </div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="telp">No. Telp</label>
										<input type="text" name="profile[no_telp]" id="no_telp" class="form-control" placeholder="No. Telp">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="email">Email</label>
										<input type="email" name="profile[email]" id="email" class="form-control" placeholder="Email">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label for="gol_darah">Gol. Darah</label>
										<input type="text" name="profile[gol_darah]" id="gol_darah" class="form-control" placeholder="Gol. Darah">
									</div>
								</div>
								<div class="col-md-6" id="div-spesialisasi">
									<div class="form-group">
										<label for="spesialisasi">Spesialisasi</label>
										<input type="text" name="profile[spesialisasi]" id="spesialisasi" class="form-control" placeholder="Spesialisasi">
									</div>
								</div>
							</div>
						</div>
          </div>

        </div>

				<div class="col-md-12">
					<div class="box box-success">

						<div class="box-header with-border">
							<h3 class="box-title">Penghargaan & Penugasan</h3>
							<div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
						</div>

						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									<label for="reward">Penghargaan</label>
									<div class="input-group control-group after-add-more-penghargaan">
										<input type="text" name="penghargaan[]" id="reward" class="form-control" placeholder="Penghargaan">
										<div class="input-group-btn">
											<button class="btn btn-success add-more-penghargaan" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="penugasan">Penugasan</label>
									<div class="input-group control-group after-add-more-penugasan">
										<input type="text" name="penugasan[]" id="penugasan" class="form-control" placeholder="Penugasan">
										<div class="input-group-btn">
											<button class="btn btn-success add-more-penugasan" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
										</div>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>

        <div class="col-md-12">
					<div class="box box-success">

						<div class="box-header with-border">
							<h3 class="box-title">Domisili Tempat Tinggal</h3>
							<div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
						</div>

						<div class="box-body">
		          <div class="row">
		            <div class="col-md-6">
		              <div class="form-group">
		                <label for="provinsi">Provinsi</label>
		                <select class="form-control" id="provinsi" name="profile[provinsi]">
		                  <option value="">Pilih Provinsi</option>
		                  <?php
											if (isset($resProvinsi))
											{
												if ($resProvinsi->num_rows() > 0)
												{
													foreach ($resProvinsi->result() as $vProvinsi)
													{
														?>
														<option value="<?php cetak($vProvinsi->id); ?>"><?php cetak($vProvinsi->provinsi); ?></option>
														<?php
													}
												}
											}
		                  ?>
		                </select>
		              </div>
		            </div>
		            <div class="col-md-6">
		              <div class="form-group">
		                <label for="kabupaten">Kabupaten/Kota</label>
		                <select class="form-control" id="kabupaten" name="profile[kabupaten]">
		                  <option value="">Pilih Kabupaten</option>
		                </select>
		              </div>
		            </div>
		          </div>

		          <div class="row">
		            <div class="col-md-6">
		              <div class="form-group">
		                <label for="kecamatan">Kecamatan</label>
		                <select class="form-control" id="kecamatan" name="profile[kecamatan]">
		                  <option value="">Pilih Kecamatan</option>
		                </select>
		              </div>
		            </div>
		            <div class="col-md-6">
		              <div class="form-group">
		                <label for="desa">Kelurahan/Desa</label>
		                <select class="form-control" id="desa" name="profile[desa]">
		                  <option value="">Pilih Desa</option>
		                </select>
		              </div>
		            </div>
							</div>
						</div>
          </div>

        </div>

        <div class="col-md-12">
					<div class="box box-success">

						<div class="box-header with-border">
							<h3 class="box-title">Unit PMI Kab-Kota</h3>
							<div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
						</div>

						<div class="box-body">
		          <div class="row">
		            <div class="col-md-4">
		              <div class="form-group">
		                <label for="provinsi">Provinsi</label>
		                <select class="form-control" id="unitprovinsi" name="userunit[provinsi]">
		                  <option value="">Pilih Provinsi</option>
											<?php
											if (isset($resProvinsi))
											{
												if ($resProvinsi->num_rows() > 0)
												{
													foreach ($resProvinsi->result() as $vProvinsi)
													{
														?>
														<option value="<?php cetak($vProvinsi->id); ?>"><?php cetak($vProvinsi->provinsi); ?></option>
														<?php
													}
												}
											}
		                  ?>
		                </select>
		              </div>
		            </div>
		            <div class="col-md-4">
		              <div class="form-group">
		                <label for="kabupaten">Kabupaten/Kota</label>
		                <select class="form-control" id="unitkabupaten" name="userunit[kabupaten]">
		                  <option value="">Pilih Kabupaten</option>
		                </select>
		              </div>
		            </div>
		            <div class="col-md-4">
		              <div class="form-group">
		                <label for="unit">Unit</label>
		                <select class="form-control" id="unitunit" name="userunit[unit]" required>
		                  <option value="">Pilih Unit</option>
		                </select>
		              </div>
		            </div>
							</div>
						</div>
          </div>

        </div>

        <div class="col-md-12">
					<div class="box box-success">

						<div class="box-header with-border">
							<h3 class="box-title">Photo</h3>
							<div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
						</div>

						<div class="box-body">
		          <div class="form-group">
		            <label for="photo">File input</label>
		            <input type="file" name="profilephoto" id="profilephoto">
		          </div>
						</div>
					</div>
        </div>

        <div class="col-md-12">
					<div class="box box-success">

						<div class="box-header with-border">
							<h3 class="box-title">Pelatihan yang Pernah Diikuti</h3>
							<div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
						</div>

						<div class="box-body">
		          <div id="pelatihan">
		          </div>
						</div>
					</div>

          <button type="submit" class="btn btn-success pull-right" name="register" id="register" value="register">Register</button>
        </div>
      </div>

		</form>
	</div>

</div>

<div class="copy-penghargaan hide">
  <div class="control-group input-group" id="penghargaan-group" style="margin-top:10px">
    <input type="text" name="penghargaan[]" class="form-control" placeholder="Penghargaan">
    <div class="input-group-btn">
      <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
    </div>
  </div>
</div>

<div class="copy-penugasan hide">
  <div class="control-group input-group" id="penugasan-group" style="margin-top:10px">
    <input type="text" name="penugasan[]" class="form-control" placeholder="Penugasan">
    <div class="input-group-btn">
      <button class="btn btn-danger remove-penugasan" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#div-spesialisasi').hide();

		$('#jenis_anggota').change(function(){
			if ($('#jenis_anggota').val() == 3) {
				$('#div-spesialisasi').show('slow');
			}
			else {
				$('#div-spesialisasi').hide('slow');
			}
		});

		$('#jenis_anggota').select2();
		$('#sub_jenis_anggota').select2();
		$('#agama').select2();
		$('#provinsi').select2();
		$('#kabupaten').select2();
		$('#kecamatan').select2();
		$('#desa').select2();
		$('#unitprovinsi').select2();
		$('#unitkabupaten').select2();
		$('#unitunit').select2();

		$('#tanggal_lahir').datepicker({
        format: "dd-mm-yyyy",
				clearBtn: true,
				autoclose: true,
		    todayHighlight: true
    });

		$('#provinsi').change(function(){
			var provinsi_id = $(this).val();
			$.ajax({
				url: '<?php echo site_url('Kabupaten/ajax_by_provinsi_id'); ?>',
				data: {provinsi_id:provinsi_id},
				dataType: 'json',
				type: 'POST',
				success: function(responce) {
					$('#kabupaten').empty();
					$('#kabupaten').append('<option value="">Pilih Kabupaten</option>');
					$.each(responce, function(key, value) {
						$('#kabupaten').append('<option value="'+value.id+'">'+value.kabupaten+'</option>');
					});
				}
			});
		});

		$('#kabupaten').change(function(){
			var kabupaten_id = $('#kabupaten').val();
			$.ajax({
				url: '<?php echo site_url('Kecamatan/ajax_by_kabupaten_id'); ?>',
				data: {kabupaten_id:kabupaten_id},
				dataType: 'json',
				type: 'POST',
				success: function(responce) {
					$('#kecamatan').empty();
					$('#kecamatan').append('<option value="">Pilih Kecamatan</option>');
					$.each(responce, function(key, value) {
						$('#kecamatan').append('<option value="'+value.id+'">'+value.kecamatan+'</option>');
					});
				}
			});
		});

		$('#kecamatan').change(function(){
			var kecamatan_id = $('#kecamatan').val();
			$.ajax({
				url: '<?php echo site_url('Desa/ajax_by_kecamatan_id'); ?>',
				data: {kecamatan_id:kecamatan_id},
				dataType: 'json',
				type: 'POST',
				success: function(responce) {
					$('#desa').empty();
					$('#desa').append('<option value="">Pilih Desa</option>');
					$.each(responce, function(key, value) {
						$('#desa').append('<option value="'+value.id+'">'+value.desa+'</option>');
					});
				}
			});
		});

		$('#jenis_anggota').change(function(){
			var jenis_anggota_id = $('#jenis_anggota').val();
			$.ajax({
				url: '<?php echo site_url('Sub_jenis_anggota/ajax_by_jenis_anggota_id'); ?>',
				data: {jenis_anggota_id:jenis_anggota_id},
				dataType: 'json',
				type: 'POST',
				success: function(responce) {
					$('#sub_jenis_anggota').empty();
					$('#sub_jenis_anggota').append('<option value="">Pilih Sub Jenis Anggota</option>');
					$.each(responce, function(key, value) {
						$('#sub_jenis_anggota').append('<option value="'+value.id+'">'+value.sub_jenis_anggota+'</option>');
					});
				}
			});
		});

		$('#unitprovinsi').change(function(){
			var provinsi_id = $('#unitprovinsi').val();
			var sub_jenis_anggota_id = $('#sub_jenis_anggota').val();
			$.ajax({
				url: '<?php echo site_url('Kabupaten/ajax_by_provinsi_id'); ?>',
				data: {
					provinsi_id:provinsi_id,
				},
				dataType: 'json',
				type: 'POST',
				success: function(responce) {
					$('#unitkabupaten').empty();
					$('#unitkabupaten').append('<option value="">Pilih Kabupaten</option>');
					$.each(responce, function(key, value) {
						$('#unitkabupaten').append('<option value="'+value.id+'">'+value.kabupaten+'</option>');
					});
				}
			});
		});

		$('#unitkabupaten').change(function(){
			var kabupaten_id = $('#unitkabupaten').val();
			var sub_jenis_anggota_id = $('#sub_jenis_anggota').val();

			if (sub_jenis_anggota_id == '') {
				alert('Pilih Sub Jenis Anggota terlebih dahulu!');
			}

			$.ajax({
				url: '<?php echo site_url('Unit/ajax_by_kabupaten_id'); ?>',
				data: {
					kabupaten_id:kabupaten_id,
					sub_jenis_anggota_id:sub_jenis_anggota_id
				},
				dataType: 'json',
				type: 'POST',
				success: function(responce) {
					$('#unitunit').empty();
					$('#unitunit').append('<option value="">Pilih Unit</option>');
					$.each(responce, function(key, value) {
						$('#unitunit').append('<option value="'+value.id+'">'+value.unit+'</option>');
					});
				}
			});
		});

		$('#jenis_anggota').change(function(){
			var jenis_anggota_id = $(this).val();
			$.ajax({
				url: '<?php echo site_url('Pelatihan/ajax_by_jenis_anggota_id'); ?>',
				type: 'POST',
				data: {jenis_anggota_id:jenis_anggota_id},
				success: function(responce) {
					$('#pelatihan').html(responce);
				}
			});
		});

		$("#my-alert").fadeTo(2000, 500).slideUp(500, function(){
		    $("#success-alert").slideUp(500);
		});

		$(".add-more-penghargaan").click(function(){
			var html = $(".copy-penghargaan").html();
			$(".after-add-more-penghargaan").after(html);
		});

		$("body").on("click",".remove",function(){
			$(this).parents("#penghargaan-group").remove();
		});

		$(".add-more-penugasan").click(function(){
			var html = $(".copy-penugasan").html();
			$(".after-add-more-penugasan").after(html);
		});

		$("body").on("click",".remove-penugasan",function(){
			$(this).parents("#penugasan-group").remove();
		});
	});
</script>
