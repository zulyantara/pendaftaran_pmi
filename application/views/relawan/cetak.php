<img src="<?php echo base_url('src/img/kop_surat.png'); ?>" alt=""><hr>
<table border="0" cellspacing="0" style="font-size: 10px; margin-bottom: 10px;" width="100%">
  <tr>
    <td width="150px">Nama</td>
    <td width="10px">:</td>
    <td width="350px"><?php echo $row->name; ?></td>
    <td rowspan="8" align="right">
      <img src="<?php echo $row->image; ?>" width="100px" alt="" style="padding: 3px; border: 3px solid #d2d6de;">
    </td>
  </tr>
  <tr>
    <td>No KTA</td>
    <td>:</td>
    <td><?php echo $row->idcard; ?></td>
  </tr>
  <tr>
    <td>Tempat, Tanggal Lahir</td>
    <td>:</td>
    <td><?php echo $row->tempat_lahir.", ".date("d-m-Y", strtotime($row->tanggal_lahir)); ?></td>
  </tr>
  <tr>
    <td>Umur</td>
    <td>:</td>
    <td><?php echo $row->umur; ?></td>
  </tr>
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td><?php echo $row->gender; ?></td>
  </tr>
  <tr>
    <td>Agama</td>
    <td>:</td>
    <td><?php echo $row->agama; ?></td>
  </tr>
  <tr>
    <td>No Telepon</td>
    <td>:</td>
    <td><?php echo $row->no_telp; ?></td>
  </tr>
  <tr>
    <td>Gol. Darah</td>
    <td>:</td>
    <td><?php echo $row->gol_darah; ?></td>
  </tr>
</table>

<?php
if ( ! empty($resPenghargaan)) {
  ?>
  <table id="table_<?php echo $modul; ?>" border="1" cellspacing="0" width="100%" style="font-size: 10px;">
    <thead>
      <tr>
        <th class="text-center" width="50px">No</th>
        <th class="text-center">Penghargaan</th>
      </tr>
    </thead>

    <tbody>
      <?php
      $no = 0;
      foreach ($resPenghargaan as $vPenghargaan) {
        ?>
        <tr>
          <td align="center"><?php echo $no = $no + 1; ?></td>
          <td><?php echo $vPenghargaan->penghargaan; ?></td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
  <br>
  <?php
}

if ( ! empty($resPenugasan)) {
  ?>
  <table id="table_<?php echo $modul; ?>" border="1" cellspacing="0" width="100%" style="font-size: 10px;">
    <thead>
      <tr>
        <th class="text-center" width="50px">No</th>
        <th class="text-center">Penugasan</th>
      </tr>
    </thead>

    <tbody>
      <?php
      $no = 0;
      foreach ($resPenugasan as $vPenugasan) {
        ?>
        <tr>
          <td align="center"><?php echo $no = $no + 1; ?></td>
          <td><?php echo $vPenugasan->penugasan; ?></td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
  <br>
  <?php
}

if ( ! empty($resPelatihan)) {
  ?>
  <table id="table_<?php echo $modul; ?>" border="1" cellspacing="0" width="100%" style="font-size: 50px;">
    <thead>
      <tr>
        <th class="text-center" width="50px">No</th>
        <th class="text-center">Pelatihan</th>
      </tr>
    </thead>

    <tbody>
      <?php
      $no = 0;
      foreach ($resPelatihan as $vPelatihan) {
        ?>
        <tr>
          <td align="center"><?php echo $no = $no + 1; ?></td>
          <td><?php echo $vPelatihan->pelatihan; ?></td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
  <?php
}
?>
