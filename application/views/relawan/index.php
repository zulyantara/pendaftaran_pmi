<div class="box box-danger box-solid">
  <div class="box-header with-border">
    <h3 class="box-title">Relawan</h3>

    <div class="box-tools pull-right">
      <a href="<?php echo site_url(''); ?>" class="btn btn-red btn-sm btn-flat" target="_blank">Tambah Data</a>
      <button type="button" title="Refresh" onclick="reload_table();" class="btn btn-red btn-sm btn-flat">Refresh</button>
    </div>
  </div>
  <div class="box-body">
    <?php
    if ( ! is_null($this->session->flashdata('alert_class')))
    {
      ?>
      <div id="alert" class="alert alert-<?php echo $this->session->flashdata('alert_class'); ?>" role="alert"><?php echo $this->session->flashdata('alert_ket'); ?></div>
      <?php
    }
    ?>

    <div class="row">
      <div class="col-md-12">
        <div class="box box-default box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Advance Search</h3>
          </div>

          <div class="box-body">
            <form class="form-inline" id="frm_search">
              <div class="form-group">
                <label class="sr-only" for="opt_jenis_anggota">Jenis Anggota</label>
                <select id="opt_jenis_anggota" class="form-control">
                  <option value="">Pilih Jenis Anggota</option>
                  <?php
                  foreach ($rJenisAnggota as $vJenisAnggota) {
                    ?>
                    <option value="<?php cetak($vJenisAnggota->id); ?>"><?php cetak($vJenisAnggota->jenis_anggota); ?></option>
                    <?php
                  }
                  ?>
                </select>
              </div>
              <div class="form-group">
                <label class="sr-only" for="opt_sub_jenis_anggota">Sub Jenis Anggota</label>
                <select id="opt_sub_jenis_anggota" class="form-control">
                  <option value="">Pilih Sub Jenis Anggota</option>
                </select>
              </div>
              <div class="form-group">
                <label class="sr-only" for="opt_kabupaten">Kabupaten</label>
                <select id="opt_kabupaten" class="form-control">
                  <option value="">Pilih Kabupaten</option>
                  <?php
                  foreach ($rKabupaten as $vKabupaten) {
                    ?>
                    <option value="<?php cetak($vKabupaten->id); ?>"><?php cetak($vKabupaten->kabupaten); ?></option>
                    <?php
                  }
                  ?>
                </select>
              </div>
              <div class="form-group">
                <label class="sr-only" for="opt_unit">Unit</label>
                <select id="opt_unit" class="form-control">
                  <option value="">Pilih Unit</option>
                </select>
              </div>
              <button type="button" id="search" class="btn btn-default">Search</button>
              <button type="button" id="btn_reset" class="btn btn-default">Reset</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <table id="table_<?php echo $modul; ?>" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th class="text-center">No</th>
          <th class="text-center">Nama</th>
          <th class="text-center">Jenis Kelamin</th>
          <th class="text-center">Jenis Anggota</th>
          <th class="text-center">Sub Jenis Anggota</th>
          <th class="text-center">Kabupaten/Kota</th>
          <th class="text-center">Unit</th>
          <th class="text-center" style="width:150px;">Action</th>
        </tr>
      </thead>

      <tbody>
      </tbody>
    </table>
  </div>
</div>

<div class="modal fade" id="modalProfile" tabindex="-1" role="dialog" aria-labelledby="modalProfileLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form class="" action="<?php echo site_url($modul.'/print_profile'); ?>" method="post" target="_blank">
        <input type="hidden" name="id_profile" id="id_profile">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Profil</h4>
        </div>
        <div class="modal-body" id="modalBody">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Print</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $("#alert").fadeTo(5000, 500).slideUp(500, function(){
    $("#alert").slideUp(500);
  });

  //datatables
  var table = $('#table_<?php echo $modul; ?>').DataTable({

    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": "<?php echo site_url($modul.'/data_grid')?>",
      "type": "POST",
      "data": function(data){
        data.jenis_anggota = $('#opt_jenis_anggota').val();
        data.sub_jenis_anggota = $('#opt_sub_jenis_anggota').val();
        data.kabupaten = $('#opt_kabupaten').val();
        data.unit = $('#opt_unit').val();
      }
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      {
        "targets": [ 0 ], //first column
        "orderable": false, //set not orderable
      },
      {
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },

    ],

  });

  //check all
  $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });

  $("#success-alert").fadeTo(5000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
  });

  $('#search').click(function(){
    table.ajax.reload();
  });

  $('#btn_reset').click(function(){
    $('#frm_search')[0].reset();
    table.ajax.reload();
  });

  $('#opt_jenis_anggota').change(function(){
    var jenis_anggota_id = $(this).val();
    $.ajax({
      url: '<?php echo site_url('Sub_jenis_anggota/ajax_by_jenis_anggota_id'); ?>',
      data: {jenis_anggota_id:jenis_anggota_id},
      dataType: 'json',
      type: 'POST',
      success: function(responce) {
        $('#opt_sub_jenis_anggota').empty();
        $('#opt_sub_jenis_anggota').append('<option value="">Pilih Sub Jenis Anggota</option>');
        $.each(responce, function(key, value) {
          $('#opt_sub_jenis_anggota').append('<option value="'+value.id+'">'+value.sub_jenis_anggota+'</option>');
        });
      }
    });
  });

  $('#opt_kabupaten').change(function(){
    var kabupaten_id = $(this).val();
    var sub_jenis_anggota_id = $('#opt_sub_jenis_anggota').val();

    // if (sub_jenis_anggota_id == '') {
    //   alert('Pilih Sub Jenis Anggota terlebih dahulu!');
    // }

    $.ajax({
      url: '<?php echo site_url('Unit/ajax_by_kabupaten_id'); ?>',
      data: {
        kabupaten_id:kabupaten_id,
        sub_jenis_anggota_id:sub_jenis_anggota_id
      },
      dataType: 'json',
      type: 'POST',
      success: function(responce) {
        $('#opt_unit').empty();
        $('#opt_unit').append('<option value="">Pilih Unit</option>');
        $.each(responce, function(key, value) {
          $('#opt_unit').append('<option value="'+value.id+'">'+value.unit+'</option>');
        });
      }
    });
  });
});

function deleteItem(i, a)
{
  if (confirm("Anda yakin menghapus data "+a+"?"))
  {
    $.ajax({
      url: '<?php echo site_url($modul.'/destroy'); ?>',
      method: 'POST',
      data: {id: i},
      success: function(response){
        alert(response);
        $('#table_<?php echo $modul; ?>').DataTable().ajax.reload();
      },
    });
  }
  return false;
}

function showProfile(id) {
  $.ajax({
    url: '<?php echo site_url($modul.'/profile'); ?>',
    method: 'POST',
    data: {id:id},
    success: function(response) {
      $('#modalProfile').modal('show');
      $('#modalBody').html(response);
    }
  });
}

function reload_table()
{
  $('#table_<?php echo $modul; ?>').DataTable().ajax.reload();
}
</script>
