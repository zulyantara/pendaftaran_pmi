<?php
$id = isset($row) ? $row->id : '';
$name = isset($row) ? $row->name : '';
$tempat_lahir = isset($row) ? $row->tempat_lahir : '';
$tanggal_lahir = isset($row) ? $row->tanggal_lahir : '';
$gender = isset($row) ? $row->gender : '';
$agama = isset($row) ? $row->agama_id : '';
$provinsi = isset($row) ? $row->provinsi_id : '';
$kabupaten = isset($row) ? $row->kabupaten_id : '';
$kecamatan = isset($row) ? $row->kecamatan_id : '';
$desa = isset($row) ? $row->desa_id : '';
$spesialisasi = isset($row) ? $row->spesialisasi : '';
$unit = isset($row) ? $row->unit_id : '';
$jenis_anggota = isset($row) ? $row->jenis_anggota_id : '';
$sub_jenis_anggota = isset($row) ? $row->sub_jenis_anggota_id : '';
$val_btn = isset($row) ? 'update' : 'save';
?>

<div class="box box-danger box-solid">
  <div class="box-header with-border">
    <h3 class="box-title">Relawan</h3>

    <div class="box-tools pull-right">
      <a href="<?php echo site_url($modul); ?>" class="btn btn-red btn-sm btn-flat">List</a>
    </div>
  </div>
  <div class="box-body">
    <form action="<?php echo site_url($modul.'/submit'); ?>" method="post" class="form-horizontal">
      <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
      <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="name" id="name" placeholder="Nama" autofocus="autofocus" value="<?php cetak($name); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="tempat_lahir" class="col-sm-2 control-label">Tempat Lahir</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" value="<?php cetak($tempat_lahir); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="tanggal_lahir" class="col-sm-2 control-label">Tanggal Lahir</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="tanggal_lahir" id="tanggal_lahir" placeholder="Tanggal Lahir" value="<?php cetak($tanggal_lahir); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="gender" class="col-sm-2 control-label">Jenis Kelamin</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="gender" id="gender" placeholder="Jenis Kelamin" value="<?php cetak($gender); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="agama" class="col-sm-2 control-label">Agama</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="agama" id="agama" placeholder="Agama" value="<?php cetak($agama); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="provinsi" class="col-sm-2 control-label">Provinsi</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="provinsi" id="provinsi" placeholder="Provinsi" value="<?php cetak($provinsi); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="kabupaten" class="col-sm-2 control-label">Kabupaten</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="kabupaten" id="kabupaten" placeholder="Kabupaten" value="<?php cetak($kabupaten); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="kecamatan" class="col-sm-2 control-label">Kecamatan</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="kecamatan" id="kecamatan" placeholder="Kecamatan" value="<?php cetak($kecamatan); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="desa" class="col-sm-2 control-label">Desa</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="desa" id="desa" placeholder="Desa" value="<?php cetak($desa); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="unit" class="col-sm-2 control-label">Unit</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="unit" id="unit" placeholder="Unit" value="<?php cetak($unit); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="spesialisasi" class="col-sm-2 control-label">Spesialisasi</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="spesialisasi" id="spesialisasi" placeholder="Spesialisasi" value="<?php cetak($spesialisasi); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="jenis_anggota" class="col-sm-2 control-label">Jenis Anggota</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="jenis_anggota" id="jenis_anggota" placeholder="Jenis Anggota" value="<?php cetak($jenis_anggota); ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="sub_jenis_anggota" class="col-sm-2 control-label">Sub Jenis Anggota</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="sub_jenis_anggota" id="sub_jenis_anggota" placeholder="Sub Jenis Anggota" value="<?php cetak($sub_jenis_anggota); ?>">
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="submit" id="submit" value="<?php echo $val_btn; ?>" class="btn btn-primary btn-flat">Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>
