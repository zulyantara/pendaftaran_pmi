<div class="row">
  <div class="col-md-4">
    <div class="box box-primary">
      <div class="box-body box-profile">
        <?php
        if ($row->image != '')
        {
          ?>
          <img class="profile-user-img img-responsive img-rounded" src="<?php echo $row->image; ?>" alt="User profile picture">
          <?php
        }
        ?>

        <h3 class="profile-username text-center"><?php cetak($row->name); ?></h3>
        <input type="hidden" name="id_profile" value="<?php cetak($row->id); ?>">

        <p class="text-muted text-center"><?php cetak($row->email); ?></p>

        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>No KTA</b> <a class="pull-right"><?php cetak($row->idcard); ?></a>
          </li>
          <li class="list-group-item">
            <b>Tempat Lahir</b> <a class="pull-right"><?php cetak($row->tempat_lahir); ?></a>
          </li>
          <li class="list-group-item">
            <b>Umur</b> <a class="pull-right"><?php cetak($row->umur); ?> Tahun</a>
          </li>
          <li class="list-group-item">
            <b>Tanggal Lahir</b> <a class="pull-right"><?php echo date('d-m-Y', strtotime($row->tanggal_lahir)); ?></a>
          </li>
          <li class="list-group-item">
            <b>Jenis Kelamin</b> <a class="pull-right"><?php cetak($row->gender); ?></a>
          </li>
          <li class="list-group-item">
            <b>Agama</b> <a class="pull-right"><?php cetak($row->agama); ?></a>
          </li>
          <li class="list-group-item">
            <b>No Telepon</b> <a class="pull-right"><?php cetak($row->no_telp); ?></a>
          </li>
          <li class="list-group-item">
            <b>Gol Darah</b> <a class="pull-right"><?php cetak($row->gol_darah); ?></a>
          </li>
          <li class="list-group-item">
            <b>Provinsi</b> <a class="pull-right"><?php cetak($row->provinsi); ?></a>
          </li>
          <li class="list-group-item">
            <b>Kabupaten</b> <a class="pull-right"><?php cetak($row->kabupaten); ?></a>
          </li>
          <li class="list-group-item">
            <b>Kecamatan</b> <a class="pull-right"><?php cetak($row->kecamatan); ?></a>
          </li>
          <li class="list-group-item">
            <b>Desa</b> <a class="pull-right"><?php cetak($row->desa); ?></a>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="col-md-8">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Penghargaan</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Penghargaan</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if ( ! empty($resPenghargaan))
            {
              $no = 0;
              foreach ($resPenghargaan as $vPenghargaan)
              {
                ?>
                <tr>
                  <td><?php echo $no = $no + 1; ?></td>
                  <td><?php cetak($vPenghargaan->penghargaan); ?></td>
                </tr>
                <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Penugasan</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Penugasan</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if ( ! empty($resPenugasan))
            {
              $no = 0;
              foreach ($resPenugasan as $vPenugasan)
              {
                ?>
                <tr>
                  <td><?php echo $no = $no + 1; ?></td>
                  <td><?php cetak($vPenugasan->penugasan); ?></td>
                </tr>
                <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Pelatihan</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Pelatihan</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if ( ! empty($resPelatihan))
            {
              $no = 0;
              foreach ($resPelatihan as $vPelatihan)
              {
                ?>
                <tr>
                  <td><?php echo $no = $no + 1; ?></td>
                  <td><?php cetak($vPelatihan->pelatihan); ?></td>
                </tr>
                <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="col-md-12">
      <!-- <a href="<?php echo site_url('Dashboard'); ?>" class="btn btn-primary btn-xl btn-flat">Dashboard</a> -->
    </div>
  </div>
</div>
