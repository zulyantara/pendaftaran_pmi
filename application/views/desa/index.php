<div class="box box-danger box-solid">
  <div class="box-header with-border">
    <h3 class="box-title">Desa</h3>

    <div class="box-tools pull-right">
      <a href="<?php echo site_url($modul.'/show'); ?>" class="btn btn-red btn-sm btn-flat">Tambah Data</a>
      <button type="button" title="Refresh" onclick="reload_table();" class="btn btn-red btn-sm btn-flat">Refresh</button>
    </div>
  </div>
  <div class="box-body table-responsive">
    <?php
    if ( ! is_null($this->session->flashdata('alert_class')))
    {
      ?>
      <div id="alert" class="alert alert-<?php echo $this->session->flashdata('alert_class'); ?>" role="alert"><?php echo $this->session->flashdata('alert_ket'); ?></div>
      <?php
    }
    ?>

    <div class="row">
      <div class="col-md-12">
        <div class="box box-default box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Advance Search</h3>
          </div>

          <div class="box-body">
            <form class="form-inline" id="frm_search">
              <div class="form-group">
                <label class="sr-only" for="opt_provinsi">Provinsi</label>
                <select id="opt_provinsi" class="form-control">
                  <option value="">Pilih Provinsi</option>
                  <?php
                  foreach ($rwProvinsi as $valProvinsi) {
                    ?>
                    <option value="<?php cetak($valProvinsi->id); ?>"><?php cetak($valProvinsi->provinsi); ?></option>
                    <?php
                  }
                  ?>
                </select>
              </div>
              <div class="form-group">
                <label class="sr-only" for="opt_kabupaten">Kabupaten</label>
                <select id="opt_kabupaten" class="form-control">
                  <option value="">Pilih Kabupaten</option>
                </select>
              </div>
              <div class="form-group">
                <label class="sr-only" for="opt_kecamatan">Kecamatan</label>
                <select id="opt_kecamatan" class="form-control">
                  <option value="">Pilih Kecamatan</option>
                </select>
              </div>
              <button type="button" id="search" class="btn btn-default">Search</button>
              <button type="button" id="btn_reset" class="btn btn-default">Reset</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <table id="table_<?php echo $modul; ?>" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th class="text-center">No</th>
          <th class="text-center">Desa/Kelurahan</th>
          <th class="text-center">Kecamatan</th>
          <th class="text-center">Kabupaten</th>
          <th class="text-center">Provinsi</th>
          <th class="text-center" style="width:150px;">Action</th>
        </tr>
      </thead>

      <tbody>
      </tbody>
    </table>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $("#alert").fadeTo(5000, 500).slideUp(500, function(){
    $("#alert").slideUp(500);
  });

  //datatables
  var table = $('#table_<?php echo $modul; ?>').DataTable({

    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": "<?php echo site_url($modul.'/data_grid')?>",
      "type": "POST",
      "data": function(data){
        data.provinsi = $('#opt_provinsi').val();
        data.kabupaten = $('#opt_kabupaten').val();
        data.kecamatan = $('#opt_kecamatan').val();
      }
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      {
        "targets": [ 0 ], //first column
        "orderable": false, //set not orderable
      },
      {
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },

    ],

  });

  //check all
  $("#check-all").click(function () {
    $(".data-check").prop('checked', $(this).prop('checked'));
  });

  $("#success-alert").fadeTo(5000, 500).slideUp(500, function(){
    $("#success-alert").slideUp(500);
  });

  $('#search').click(function(){
    table.ajax.reload();
  });

  $('#btn_reset').click(function(){
    $('#frm_search')[0].reset();
    table.ajax.reload();
  });

  $('#opt_provinsi').change(function(){
    var provinsi_id = $(this).val();
    $.ajax({
      url: '<?php echo site_url('Kabupaten/ajax_by_provinsi_id'); ?>',
      data: {provinsi_id:provinsi_id},
      dataType: 'json',
      type: 'POST',
      success: function(responce) {
        $('#opt_kabupaten').empty();
        $('#opt_kabupaten').append('<option value="">Pilih Kabupaten</option>');
        $.each(responce, function(key, value) {
          $('#opt_kabupaten').append('<option value="'+value.id+'">'+value.kabupaten+'</option>');
        });
      }
    });
  });

  $('#opt_kabupaten').change(function(){
    var kabupaten_id = $('#opt_kabupaten').val();
    $.ajax({
      url: '<?php echo site_url('Kecamatan/ajax_by_kabupaten_id'); ?>',
      data: {kabupaten_id:kabupaten_id},
      dataType: 'json',
      type: 'POST',
      success: function(responce) {
        $('#opt_kecamatan').empty();
        $('#opt_kecamatan').append('<option value="">Pilih Kecamatan</option>');
        $.each(responce, function(key, value) {
          $('#opt_kecamatan').append('<option value="'+value.id+'">'+value.kecamatan+'</option>');
        });
      }
    });
  });
});

function deleteItem(i, a)
{
  if (confirm("Anda yakin menghapus data "+a+"?"))
  {
    $.ajax({
      url: '<?php echo site_url($modul.'/destroy'); ?>',
      method: 'POST',
      data: {id: i},
      success: function(response){
        alert(response);
        $('#table_<?php echo $modul; ?>').DataTable().ajax.reload();
      },
    });
  }
  return false;
}

function reload_table()
{
  $('#table_<?php echo $modul; ?>').DataTable().ajax.reload();
}
</script>
