<?php
$id = isset($row) ? $row->id : '';
$desa = isset($row) ? $row->desa : '';
$provinsi = isset($row) ? $row->provinsi_id : '';
$kabupaten = isset($row) ? $row->kabupaten_id : '';
$kecamatan = isset($row) ? $row->kecamatan_id : '';
$val_btn = isset($row) ? 'update' : 'save';
?>

<div class="box box-danger box-solid">
  <div class="box-header with-border">
    <h3 class="box-title">Desa/Kelurahan</h3>

    <div class="box-tools pull-right">
      <a href="<?php echo site_url($modul); ?>" class="btn btn-red btn-sm btn-flat">List</a>
    </div>
  </div>
  <div class="box-body">
    <form action="<?php echo site_url($modul.'/submit'); ?>" method="post" class="form-horizontal">
      <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
      <div class="form-group">
        <label for="provinsi" class="col-sm-2 control-label">Provinsi</label>
        <div class="col-sm-10">
          <select name="provinsi" id="provinsi" class="form-control">
            <option value="">Pilih Provinsi</option>
            <?php
            if (isset($rwProvinsi)) {
              foreach ($rwProvinsi as $valProvinsi) {
                $selProvinsi = $valProvinsi->id == $provinsi ? 'selected="selected"' : '';
                ?>
                <option value="<?php cetak($valProvinsi->id); ?>" <?php echo $selProvinsi; ?>><?php cetak($valProvinsi->provinsi); ?></option>
                <?php
              }
            }
            ?>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="kabupaten" class="col-sm-2 control-label">Kabupaten</label>
        <div class="col-sm-10">
          <select name="kabupaten" id="kabupaten" class="form-control">
            <option value="">Pilih Kabupaten</option>
            <?php
            if (isset($rwKabupaten)) {
              foreach ($rwKabupaten as $valKabupaten) {
                $selKabupaten = $valKabupaten->id == $kabupaten ? 'selected="selected"' : '';
                ?>
                <option value="<?php cetak($valKabupaten->id); ?>" <?php echo $selKabupaten; ?>><?php cetak($valKabupaten->kabupaten); ?></option>
                <?php
              }
            }
            ?>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="kecamatan" class="col-sm-2 control-label">Kecamatan</label>
        <div class="col-sm-10">
          <select name="kecamatan" id="kecamatan" class="form-control">
            <option value="">Pilih Kecamatan</option>
            <?php
            if (isset($rwKecamatan)) {
              foreach ($rwKecamatan as $valKecamatan) {
                $selKecamatan = $valKecamatan->id == $kecamatan ? 'selected="selected"' : '';
                ?>
                <option value="<?php cetak($valKecamatan->id); ?>" <?php echo $selKecamatan; ?>><?php cetak($valKecamatan->kecamatan); ?></option>
                <?php
              }
            }
            ?>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="desa" class="col-sm-2 control-label">Desa/Kelurahan</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="desa" id="desa" placeholder="Desa/Kelurahan" autofocus="autofocus" value="<?php cetak($desa); ?>">
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="submit" id="submit" value="<?php echo $val_btn; ?>" class="btn btn-primary btn-flat">Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $('#provinsi').change(function(){
    var provinsi_id = $(this).val();
    $.ajax({
      url: '<?php echo site_url('Kabupaten/ajax_by_provinsi_id'); ?>',
      data: {provinsi_id:provinsi_id},
      dataType: 'json',
      type: 'POST',
      success: function(responce) {
        $('#kabupaten').empty();
        $('#kabupaten').append('<option value="">Pilih Kabupaten</option>');
        $.each(responce, function(key, value) {
          $('#kabupaten').append('<option value="'+value.id+'">'+value.kabupaten+'</option>');
        });
      }
    });
  });

  $('#kabupaten').change(function(){
    var kabupaten_id = $('#kabupaten').val();
    $.ajax({
      url: '<?php echo site_url('Kecamatan/ajax_by_kabupaten_id'); ?>',
      data: {kabupaten_id:kabupaten_id},
      dataType: 'json',
      type: 'POST',
      success: function(responce) {
        $('#kecamatan').empty();
        $('#kecamatan').append('<option value="">Pilih Kecamatan</option>');
        $.each(responce, function(key, value) {
          $('#kecamatan').append('<option value="'+value.id+'">'+value.kecamatan+'</option>');
        });
      }
    });
  });
});
</script>
