<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Auth extends CI_Controller
{
  private $modul = 'Auth';

  function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    if($this->session->userdata('isLoggedIn') !== TRUE)
    {
      $this->load->view('login/index');
    }
    else
    {
      redirect('Dashboard');
    }
  }

  function validate_credential()
  {
    if($this->input->post('btn_login') === 'btn_login')
    {
      $this->load->model('auth_model');

      $email = $this->input->post('txt_email');
      $password = $this->input->post('txt_password');

      $query = $this->auth_model->validate($email);
      // var_dump($query);exit;
      if( ! is_null($query))
      {
        if (password_verify($password, $query->row()->password)) {
          $data = array(
            'userId' => $query->row()->id,
            'userName' => $query->row()->name,
            'userEmail' => $query->row()->email,
            'userActive' => $query->row()->active,
            'isLoggedIn' => TRUE
          );

          $this->session->set_userdata($data);
          // $this->db->last_query();exit();
          redirect("Dashboard");
        }
        else {
          redirect("auth");
        }
      }
      else
      {
        redirect("auth");
      }
    }
    else
    {
      $this->index();
    }
  }

  function logout()
  {
    $this->session->sess_destroy();
    redirect('Dashboard');
  }
}
