<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Relawan extends CI_Controller
{
  var $modul = 'Relawan';

  function __construct()
  {
    parent::__construct();
    $this->load->model('User_profile_model', 'userProfile');
    $this->load->model('User_pelatihan_model', 'userPelatihan');
    $this->load->model('User_penghargaan_model', 'userPenghargaan');
    $this->load->model('User_penugasan_model', 'userPenugasan');
    $this->load->model('V_user_profile_model', 'vUserProfile');
    $this->load->model('V_user_pelatihan_model', 'vUserPelatihan');
    $this->load->model('Jenis_anggota_model', 'jenisAnggota');
    $this->load->model('Kabupaten_model', 'kabupaten');
    $this->load->model('Unit_model', 'unit');
  }

  function index()
  {
    $rJenisAnggota = $this->jenisAnggota->get_data(NULL)->result();
    $data['rJenisAnggota'] = $rJenisAnggota;

    $rKabupaten = $this->kabupaten->get_data(NULL)->result();
    $data['rKabupaten'] = $rKabupaten;

    $data["modul"] = $this->modul;
		$data["bt"]= "List ".humanize($this->modul); /* Box Title */

    $this->template->load("template/backend", strtolower($this->modul)."/index", $data);
  }

  public function show($id = NULL)
  {
    if ($this->session->userdata('isLoggedIn') == TRUE) {
      if (is_null($id)) {
        $data["modul"] = $this->modul;
        $data["bt"]= "List ".humanize($this->modul); /* Box Title */

        $this->template->load("template/backend", strtolower($this->modul)."/form", $data);
      }
      else {
        $params['id'] = $id;
        $row = $this->vUserProfile->get_data($params);
        $data['row'] = $row->row();

        $data["modul"] = $this->modul;
        $data["bt"]= "List ".humanize($this->modul); /* Box Title */

        $this->template->load("template/backend", strtolower($this->modul)."/form", $data);
      }
    }
  }

  public function submit()
  {
    if ($this->session->userdata('isLoggedIn') == TRUE) {
      $btn_submit = $this->input->post('submit');
      $id = $this->input->post('id');
      $jenis_anggota = $this->input->post('jenis_anggota');

      if ($btn_submit === 'save') {
        $params['where']['jenis_anggota'] = $jenis_anggota;
        $rowCheck = $this->jenisAnggota->get_data($params);

        if ($rowCheck->num_rows() > 0) {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_ket"] = "Data ".strtoupper($jenis_anggota)." sudah ada";
          $this->session->set_flashdata($flashdata);

          redirect($this->modul);
        }

        $paramsMax['select_max'] = 'kode';
        $rowMax = $this->jenisAnggota->get_data($paramsMax);

        $newKode = $rowMax->row()->kode;
        $kode = str_pad($newKode + 1, 2, '0', STR_PAD_LEFT);

        $data['kode'] = $kode;
        $data['jenis_anggota'] = trim(strtoupper($jenis_anggota));
        $data['created_at'] = date('Y-m-d');

        $simpan = $this->jenisAnggota->create($data);

        if ($simpan == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_ket"] = "Data ".strtoupper($jenis_anggota)." berhasil disimpan";
          $this->session->set_flashdata($flashdata);
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_ket"] = "Data ".strtoupper($jenis_anggota)." tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
        }

        redirect($this->modul);
      }
      elseif ($btn_submit === 'update') {
        $params['where']['id'] = $id;
        $params['data']['jenis_anggota'] = trim(strtoupper($jenis_anggota));
        $params['data']['updated_at'] = date('Y-m-d');

        $simpan = $this->jenisAnggota->update($params);

        if ($simpan == 1) {
          $flashdata["alert_class"] = "success";
          $flashdata["alert_ket"] = "Data ".strtoupper($jenis_anggota)." berhasil disimpan";
          $this->session->set_flashdata($flashdata);
        }
        else {
          $flashdata["alert_class"] = "warning";
          $flashdata["alert_ket"] = "Data ".strtoupper($jenis_anggota)." tidak berhasil disimpan";
          $this->session->set_flashdata($flashdata);
        }

        redirect($this->modul);
      }
      else {
        redirect($this->modul);
      }
    }
  }

  public function destroy()
  {
    if ($this->session->userdata('isLoggedIn') == TRUE) {
      $id = $this->input->post('id');

      $pUserProfile['where']['user_profile_id'] = $id;
      $this->userPelatihan->delete($pUserProfile);

      $params['where']['id'] = $id;
      $this->userProfile->delete($params);

      echo 'Data berhasil dihapus';
    }
  }

  public function data_grid()
  {
    $list = $this->vUserProfile->get_datatables($this->input->post());
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      if ($this->session->userdata('isLoggedIn') === TRUE) {
        $btn_edit = "<a href=\"".site_url($this->modul."/show/".$rw->id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>";

        $btn_delete = "<button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->id."','".$rw->name."');\">Delete</button>";

        $btn_profile = "<button type=\"button\" title=\"Profil Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"showProfile('".$rw->id."');\">Profil</button>";

        $btn_penghargaan = "<button type=\"button\" title=\"Penghargaan Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"showPenghargaan('".$rw->id."');\">Penghargaan</button>";

        $btn_penugasan = "<button type=\"button\" title=\"Penugasan Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"showPenugasan('".$rw->id."');\">Penugasan</button>";

        $btn_pelatihan = "<button type=\"button\" title=\"Pelatihan Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"showPelatihan('".$rw->id."');\">Pelatihan</button>";
      }
      else {
        $btn_edit = '';
        $btn_delete = '';
        $btn_profile = '';
      }

      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = $rw->name;
      $row[] = $rw->gender;
      $row[] = $rw->jenis_anggota;
      $row[] = $rw->sub_jenis_anggota;
      $row[] = $rw->kabupaten_unit;
      $row[] = $rw->unit;
      $row[] = "<div class=\"text-center\"><div class=\"btn-group\" role=\"group\" aria-label=\"...\">$btn_edit $btn_delete $btn_profile</div></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->vUserProfile->count_all(),
      "recordsFiltered" => $this->vUserProfile->count_filtered($this->input->post()),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }

  public function profile()
  {
    if ($this->session->userdata('isLoggedIn') == TRUE) {
      $id = $this->input->post('id');

      $wUProfile['id'] = $id;
      $rUProfile = $this->vUserProfile->get_data($wUProfile)->row();

      $wUPenghargaan['user_profile_id'] = $rUProfile->id;
      $rUPenghargaan = $this->userPenghargaan->get_data($wUPenghargaan)->result();

      $wUPenugasan['user_profile_id'] = $rUProfile->id;
      $rUPenugasan = $this->userPenugasan->get_data($wUPenugasan)->result();

      $wUPelatihan['user_profile_id'] = $rUProfile->id;
      $rUPelatihan = $this->vUserPelatihan->get_data($wUPelatihan)->result();

      $data['row'] = $rUProfile;
      $data['resPelatihan'] = $rUPelatihan;
      $data['resPenghargaan'] = $rUPenghargaan;
      $data['resPenugasan'] = $rUPenugasan;

      echo $this->load->view('detail', $data, TRUE);
    }
  }

  public function print_profile()
  {
    if ($this->session->userdata('isLoggedIn') == TRUE) {
      $id_profile = $this->input->post('id_profile');

      $where['id'] = $id_profile;
      $result = $this->vUserProfile->get_data($where);
      // var_dump($result->row());

      $wUPenghargaan['user_profile_id'] = $result->row()->id;
      $rUPenghargaan = $this->userPenghargaan->get_data($wUPenghargaan)->result();

      $wUPenugasan['user_profile_id'] = $result->row()->id;
      $rUPenugasan = $this->userPenugasan->get_data($wUPenugasan)->result();

      $wUPelatihan['user_profile_id'] = $result->row()->id;
      $rUPelatihan = $this->vUserPelatihan->get_data($wUPelatihan)->result();

      $data['row'] = $result->row();
      $data['resPelatihan'] = $rUPelatihan;
      $data['resPenghargaan'] = $rUPenghargaan;
      $data['resPenugasan'] = $rUPenugasan;

      $html = $this->load->view('relawan/cetak', $data, TRUE);

      $mpdf = new \Mpdf\Mpdf();
      $mpdf->WriteHTML($html);
      $mpdf->Output('profile_relawan.pdf', 'I');
    }
  }
}
