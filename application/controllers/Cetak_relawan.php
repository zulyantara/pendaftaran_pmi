<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Cetak_relawan extends CI_Controller
{
  var $modul = 'Cetak_relawan';

  function __construct()
  {
    parent::__construct();
    $this->load->model('User_profile_model', 'userProfile');
    $this->load->model('V_user_profile_model', 'vUserProfile');
    $this->load->model('V_user_pelatihan_model', 'vUserPelatihan');
    $this->load->model('Jenis_anggota_model', 'jenisAnggota');
    $this->load->model('Kabupaten_model', 'kabupaten');
    $this->load->model('Unit_model', 'unit');
  }

  function index()
  {
    $rJenisAnggota = $this->jenisAnggota->get_data(NULL)->result();
    $data['rJenisAnggota'] = $rJenisAnggota;

    $rKabupaten = $this->kabupaten->get_data(NULL)->result();
    $data['rKabupaten'] = $rKabupaten;

    $data["modul"] = $this->modul;
		$data["bt"]= "List ".humanize($this->modul); /* Box Title */

    $this->template->load("template/backend", strtolower($this->modul)."/index", $data);
  }

  public function show()
  {
    $id_jenis_anggota = $this->input->post('id_jenis_anggota');
    $id_sub_jenis_anggota = $this->input->post('id_sub_jenis_anggota');
    $id_kabupaten = $this->input->post('id_kabupaten');
    $id_unit = $this->input->post('id_unit');
    $cetak = $this->input->post('cetak');

    $data['id_jenis_anggota'] = $id_jenis_anggota;
    $data['id_sub_jenis_anggota'] = $id_sub_jenis_anggota;
    $data['id_kabupaten'] = $id_kabupaten;
    $data['id_unit'] = $id_unit;

    if ( ! empty($id_jenis_anggota)) {
      $where['jenis_anggota_id'] = $id_jenis_anggota;
    }
    if ( ! empty($id_sub_jenis_anggota)) {
      $where['sub_jenis_anggota_id'] = $id_sub_jenis_anggota;
    }
    if ( ! empty($id_kabupaten)) {
      $where['kabupaten_id'] = $id_kabupaten;
    }
    if ( ! empty($id_unit)) {
      $where['unit_id'] = $id_unit;
    }

    if (empty($id_jenis_anggota) && empty($id_sub_jenis_anggota && empty($id_kabupaten && empty($id_unit)))) {
      $where = FALSE;
    }

    $rwUserProfile = $this->vUserProfile->get_data($where);

    $data['rwUserProfile'] = $rwUserProfile->result();
    $data["modul"] = $this->modul;

    $html = $this->load->view('cetak_relawan/cetak', $data, TRUE);

    echo $html;
  }

  public function cetak($id_jenis_anggota = NULL, $id_sub_jenis_anggota = NULL, $id_kabupaten = NULL, $id_unit = NULL)
  {
    $data['id_jenis_anggota'] = $id_jenis_anggota;
    $data['id_sub_jenis_anggota'] = $id_sub_jenis_anggota;
    $data['id_kabupaten'] = $id_kabupaten;
    $data['id_unit'] = $id_unit;

    if (is_null($id_jenis_anggota) && is_null($id_sub_jenis_anggota) && is_null($id_kabupaten) && is_null($id_unit)) {
      $where = FALSE;
    }

    ( ! is_null($id_jenis_anggota)) ? $where['jenis_anggota_id'] = $id_jenis_anggota : '';
    ( ! is_null($id_sub_jenis_anggota)) ? $where['sub_jenis_anggota_id'] = $id_sub_jenis_anggota : '';
    ( ! is_null($id_kabupaten)) ? $where['kabupaten_id'] = $id_kabupaten : '';
    ( ! is_null($id_unit)) ? $where['unit_id'] = $id_unit : '';
    $rwUserProfile = $this->vUserProfile->get_data($where);

    $data['rwUserProfile'] = $rwUserProfile->result();
    $data["modul"] = $this->modul;

    $html = $this->load->view('cetak_relawan/cetak', $data, TRUE);

    $mpdf = new \Mpdf\Mpdf();
    $mpdf->WriteHTML($html);
    $mpdf->Output('cetak_relawan.pdf', 'I');
  }
}
