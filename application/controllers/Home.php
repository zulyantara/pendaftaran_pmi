<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jenis_anggota_model', 'jam');
		$this->load->model('Provinsi_model', 'pm');
		$this->load->model('Kabupaten_model', 'km');
		$this->load->model('Agama_model', 'am');
	}

	public function index()
	{
		$resJenisAnggota = $this->jam->get_data(NULL);
		$data['resJenisAnggota'] = $resJenisAnggota;

		$resProvinsi = $this->pm->get_data(NULL);
		$data['resProvinsi'] = $resProvinsi;

		$resKabupaten = $this->km->get_data(NULL);
		$data['resKabupaten'] = $resKabupaten;

		$resAgama = $this->am->get_data();
		$data['resAgama'] = $resAgama;

		$this->template->load('template/frontend', 'pendaftaran', $data);
	}
}
