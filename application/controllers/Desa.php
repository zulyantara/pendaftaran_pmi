<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Desa extends CI_Controller
{
  var $modul = 'Desa';
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Desa_model', 'desa');
    $this->load->model('Kecamatan_model', 'kecamatan');
    $this->load->model('Kabupaten_model', 'kabupaten');
    $this->load->model('provinsi_model', 'provinsi');
    $this->load->model('V_desa_model', 'vDesa');
  }

  function index()
  {
    $rwProvinsi = $this->provinsi->get_data(NULL);
    $data['rwProvinsi'] = $rwProvinsi->result();

    $data["modul"] = $this->modul;
		$data["bt"]= "List ".humanize($this->modul); /* Box Title */

    $this->template->load("template/backend", strtolower($this->modul)."/index", $data);
  }

  public function show($id = NULL)
  {
    $rwProvinsi = $this->provinsi->get_data(NULL);
    $data['rwProvinsi'] = $rwProvinsi->result();

    if (is_null($id)) {
      $data["modul"] = $this->modul;
      $data["bt"]= "List ".humanize($this->modul); /* Box Title */

      $this->template->load("template/backend", strtolower($this->modul)."/form", $data);
    }
    else {
      $params['where']['id'] = $id;
      $row = $this->vDesa->get_data($params)->row();
      $data['row'] = $row;

      $pKabupaten['where']['provinsi_id'] = $row->provinsi_id;
      $rwKabupaten = $this->kabupaten->get_data($pKabupaten);
      $data['rwKabupaten'] = $rwKabupaten->result();

      $pKecamatan['where']['kabupaten_id'] = $row->kabupaten_id;
      $rwKecamatan = $this->kecamatan->get_data($pKecamatan);
      $data['rwKecamatan'] = $rwKecamatan->result();

      $data["modul"] = $this->modul;
      $data["bt"]= "List ".humanize($this->modul); /* Box Title */

      $this->template->load("template/backend", strtolower($this->modul)."/form", $data);
    }
  }

  public function submit()
  {
    $btn_submit = $this->input->post('submit');
    $id = $this->input->post('id');
    $desa = $this->input->post('desa');
    $kecamatan = $this->input->post('kecamatan');

    if ($btn_submit === 'save') {
      $params['where']['desa'] = $desa;
      $params['where']['kecamatan_id'] = $kecamatan;
      $rowCheck = $this->desa->get_data($params);

      if ($rowCheck->num_rows() > 0) {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($desa)." sudah ada";
        $this->session->set_flashdata($flashdata);

        redirect($this->modul);
      }

      $data['desa'] = trim(strtoupper($desa));
      $data['kecamatan_id'] = trim(strtoupper($kecamatan));
      $data['created_at'] = date('Y-m-d');

      $simpan = $this->desa->create($data);

      if ($simpan == 1) {
        $flashdata["alert_class"] = "success";
        $flashdata["alert_ket"] = "Data ".strtoupper($desa)." berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($desa)." tidak berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }

      redirect($this->modul);
    }
    elseif ($btn_submit === 'update') {
      $params['where']['id'] = $id;
      $params['data']['desa'] = trim(strtoupper($desa));
      $params['data']['kecamatan_id'] = trim(strtoupper($kecamatan));
      $params['data']['updated_at'] = date('Y-m-d');

      $simpan = $this->desa->update($params);

      if ($simpan == 1) {
        $flashdata["alert_class"] = "success";
        $flashdata["alert_ket"] = "Data ".strtoupper($desa)." berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($desa)." tidak berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }

      redirect($this->modul);
    }
    else {
      redirect($this->modul);
    }
  }

  public function destroy()
  {
    $id = $this->input->post('id');
    $params['where']['id'] = $id;
    $this->desa->delete($params);
    echo 'Data berhasil dihapus';
  }

  public function data_grid()
  {
    $list = $this->vDesa->get_datatables($this->input->post());
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $btn_edit = "<a href=\"".site_url($this->modul."/show/".$rw->id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>";

      $btn_delete = "<button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->id."','".$rw->desa."');\">Delete</button>";

      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = strtoupper($rw->desa);
      $row[] = strtoupper($rw->kecamatan);
      $row[] = strtoupper($rw->kabupaten);
      $row[] = strtoupper($rw->provinsi);
      $row[] = "<div class=\"text-center\">$btn_edit $btn_delete</div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->vDesa->count_all(),
      "recordsFiltered" => $this->vDesa->count_filtered($this->input->post()),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }

  public function ajax_by_kecamatan_id()
  {
    $params['where']['kecamatan_id'] = $this->input->post('kecamatan_id');
    $result = $this->desa->get_data($params);

    if ($result->num_rows() > 0)
    {
      echo json_encode($result->result());
    }
    else
    {
      echo json_last_error_msg();
    }
  }
}
