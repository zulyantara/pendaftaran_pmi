<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Jenis_anggota extends CI_Controller
{
  var $modul = 'Jenis_anggota';

  function __construct()
  {
    parent::__construct();
    $this->load->model('Jenis_anggota_model', 'jenisAnggota');
  }

  function index()
  {
    $data["modul"] = $this->modul;
		$data["bt"]= "List ".humanize($this->modul); /* Box Title */

    $this->template->load("template/backend", strtolower($this->modul)."/index", $data);
  }

  public function show($id = NULL)
  {
    if (is_null($id)) {
      $data["modul"] = $this->modul;
      $data["bt"]= "List ".humanize($this->modul); /* Box Title */
  
      $this->template->load("template/backend", strtolower($this->modul)."/form", $data);
    }
    else {
      $params['where']['id'] = $id;
      $row = $this->jenisAnggota->get_data($params);
      $data['row'] = $row->row();

      $data["modul"] = $this->modul;
      $data["bt"]= "List ".humanize($this->modul); /* Box Title */
  
      $this->template->load("template/backend", strtolower($this->modul)."/form", $data);
    }
  }

  public function submit()
  {
    $btn_submit = $this->input->post('submit');
    $id = $this->input->post('id');
    $jenis_anggota = $this->input->post('jenis_anggota');

    if ($btn_submit === 'save') {
      $params['where']['jenis_anggota'] = $jenis_anggota;
      $rowCheck = $this->jenisAnggota->get_data($params);

      if ($rowCheck->num_rows() > 0) {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($jenis_anggota)." sudah ada";
        $this->session->set_flashdata($flashdata);
        
        redirect($this->modul);
      }

      $paramsMax['select_max'] = 'kode';
      $rowMax = $this->jenisAnggota->get_data($paramsMax);
      
      $newKode = $rowMax->row()->kode;
      $kode = str_pad($newKode + 1, 2, '0', STR_PAD_LEFT);

      $data['kode'] = $kode;
      $data['jenis_anggota'] = trim(strtoupper($jenis_anggota));
      $data['created_at'] = date('Y-m-d');

      $simpan = $this->jenisAnggota->create($data);

      if ($simpan == 1) {
        $flashdata["alert_class"] = "success";
        $flashdata["alert_ket"] = "Data ".strtoupper($jenis_anggota)." berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($jenis_anggota)." tidak berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      
      redirect($this->modul);
    }
    elseif ($btn_submit === 'update') {
      $params['where']['id'] = $id;
      $params['data']['jenis_anggota'] = trim(strtoupper($jenis_anggota));
      $params['data']['updated_at'] = date('Y-m-d');

      $simpan = $this->jenisAnggota->update($params);

      if ($simpan == 1) {
        $flashdata["alert_class"] = "success";
        $flashdata["alert_ket"] = "Data ".strtoupper($jenis_anggota)." berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($jenis_anggota)." tidak berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      
      redirect($this->modul);
    }
    else {
      redirect($this->modul);
    }
  }

  public function destroy()
  {
    $id = $this->input->post('id');
    $params['where']['id'] = $id;
    $this->jenisAnggota->delete($params);
    echo 'Data berhasil dihapus';
  }

  public function data_grid()
  {
    $list = $this->jenisAnggota->get_datatables($this->input->post());
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $btn_edit = "<a href=\"".site_url($this->modul."/show/".$rw->id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>";

      $btn_delete = "<button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->id."','".$rw->jenis_anggota."');\">Delete</button>";

      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = strtoupper($rw->kode);
      $row[] = strtoupper($rw->jenis_anggota);
      $row[] = "<div class=\"text-center\">$btn_edit $btn_delete</div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->jenisAnggota->count_all(),
      "recordsFiltered" => $this->jenisAnggota->count_filtered($this->input->post()),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }
}
