<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Kecamatan extends CI_Controller
{
  var $modul = 'Kecamatan';
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Kecamatan_model', 'kecamatan');
    $this->load->model('Kabupaten_model', 'kabupaten');
    $this->load->model('provinsi_model', 'provinsi');
    $this->load->model('V_kecamatan_model', 'vKecamatan');
  }

  function index()
  {
    $rwProvinsi = $this->provinsi->get_data(NULL);
    $data['rwProvinsi'] = $rwProvinsi->result();

    $rwKabupaten = $this->kabupaten->get_data(NULL);
    $data['rwPKabupaten'] = $rwKabupaten->result();

    $data["modul"] = $this->modul;
		$data["bt"]= "List ".humanize($this->modul); /* Box Title */

    $this->template->load("template/backend", strtolower($this->modul)."/index", $data);
  }

  public function show($id = NULL)
  {
    $rwProvinsi = $this->provinsi->get_data(NULL);
    $data['rwProvinsi'] = $rwProvinsi->result();

    $rwKabupaten = $this->kabupaten->get_data(NULL);
    $data['rwKabupaten'] = $rwKabupaten->result();

    if (is_null($id)) {
      $data["modul"] = $this->modul;
      $data["bt"]= "List ".humanize($this->modul); /* Box Title */

      $this->template->load("template/backend", strtolower($this->modul)."/form", $data);
    }
    else {
      $params['where']['id'] = $id;
      $row = $this->kecamatan->get_data($params);
      $data['row'] = $row->row();

      $data["modul"] = $this->modul;
      $data["bt"]= "List ".humanize($this->modul); /* Box Title */

      $this->template->load("template/backend", strtolower($this->modul)."/form", $data);
    }
  }

  public function submit()
  {
    $btn_submit = $this->input->post('submit');
    $id = $this->input->post('id');
    $kecamatan = $this->input->post('kecamatan');
    $kabupaten = $this->input->post('kabupaten');

    if ($btn_submit === 'save') {
      $params['where']['kecamatan'] = $kecamatan;
      $params['where']['kabupaten_id'] = $kabupaten;
      $rowCheck = $this->kecamatan->get_data($params);

      if ($rowCheck->num_rows() > 0) {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($kecamatan)." sudah ada";
        $this->session->set_flashdata($flashdata);

        redirect($this->modul);
      }

      $data['kecamatan'] = trim(strtoupper($kecamatan));
      $data['kabupaten_id'] = trim($kabupaten);
      $data['created_at'] = date('Y-m-d');

      $simpan = $this->kecamatan->create($data);

      if ($simpan == 1) {
        $flashdata["alert_class"] = "success";
        $flashdata["alert_ket"] = "Data ".strtoupper($kecamatan)." berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($unit)." tidak berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }

      redirect($this->modul);
    }
    elseif ($btn_submit === 'update') {
      $params['where']['id'] = $id;
      $params['data']['kecamatan'] = trim(strtoupper($kecamatan));
      $params['data']['kabupaten_id'] = trim($kabupaten);
      $params['data']['updated_at'] = date('Y-m-d');

      $simpan = $this->kecamatan->update($params);

      if ($simpan == 1) {
        $flashdata["alert_class"] = "success";
        $flashdata["alert_ket"] = "Data ".strtoupper($unit)." berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($kecamatan)." tidak berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }

      redirect($this->modul);
    }
    else {
      redirect($this->modul);
    }
  }

  public function destroy()
  {
    $id = $this->input->post('id');
    $params['where']['id'] = $id;
    $this->kecamatan->delete($params);
    echo 'Data berhasil dihapus';
  }

  public function data_grid()
  {
    $list = $this->vKecamatan->get_datatables($this->input->post());
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $btn_edit = "<a href=\"".site_url($this->modul."/show/".$rw->id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>";

      $btn_delete = "<button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->id."','".$rw->kecamatan."');\">Delete</button>";

      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = strtoupper($rw->kecamatan);
      $row[] = strtoupper($rw->kabupaten);
      $row[] = strtoupper($rw->provinsi);
      $row[] = "<div class=\"text-center\">$btn_edit $btn_delete</div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->vKecamatan->count_all(),
      "recordsFiltered" => $this->vKecamatan->count_filtered($this->input->post()),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }

  public function ajax_by_kabupaten_id()
  {
    $params['where']['kabupaten_id'] = $this->input->post('kabupaten_id');
    $resKecamatan = $this->kecamatan->get_data($params);

    if ($resKecamatan->num_rows() > 0)
    {
      echo json_encode($resKecamatan->result());
    }
    else
    {
      echo json_last_error_msg();
    }
  }
}
