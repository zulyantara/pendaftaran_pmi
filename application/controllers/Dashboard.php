<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Dashboard_model', 'dm');
    $this->load->model('User_profile_model', 'upm');
    $this->load->model('V_user_profile_model', 'vupm');
    $this->load->model('V_user_pelatihan_model', 'vuplm');
  }

  function index()
  {
    $wPMR['kode'] = '01';
    $rPMR = $this->dm->count_total_by_jenis_anggota($wPMR);
    // $data['total_pmr'] = $rPMR->num_rows() > 0 ? $rPMR->row()->jml : 0;
    $data['total_pmr'] = 1969+8718+5094;
    $data['total_mula'] = 1969;
    $data['total_madya'] = 8718;
    $data['total_wira'] = 5094;

    $wKSR['kode'] = '02';
    $rKSR = $this->dm->count_total_by_jenis_anggota($wKSR);
    // $data['total_ksr'] = $rKSR->num_rows() > 0 ? $rKSR->row()->jml : 0;
    $data['total_ksr'] = 2754;
    $data['total_ksr_m'] = 1221;
    $data['total_ksr_f'] = 1533;

    $wTSR['kode'] = '03';
    $rTSR = $this->dm->count_total_by_jenis_anggota($wTSR);
    // $data['total_tsr'] = $rTSR->num_rows() > 0 ? $rTSR->row()->jml : 0;
    $data['total_tsr'] = 2754;
    $data['total_tsr_m'] = 1221;
    $data['total_tsr_f'] = 1533;

    $wAnggota['kode'] = '00';
    $rAnggota = $this->dm->count_total_by_jenis_anggota($wAnggota);
    // $data['total_anggota'] = $rAnggota->num_rows() > 0 ? $rAnggota->row()->jml : 0;
    $data['total_anggota'] = 160;
    $data['total_anggota_m'] = 105;
    $data['total_anggota_f'] = 55;

    $this->template->load('template/backend', 'dashboard', $data);
  }

  function jakarta_barat()
  {
    $wPMR['jenis_anggota_kode'] = '01';
    $wPMR['kabupaten_id'] = 1;
    $rPMR = $this->dm->count_total_by_kabupaten($wPMR);
    // $data['total_pmr'] = $rPMR->num_rows() > 0 ? $rPMR->row()->jml : 0;
    $data['total_pmr'] = 365+3800+1720;
    $data['total_mula'] = 365;
    $data['total_madya'] = 3800;
    $data['total_wira'] = 1720;

    $wKSR['jenis_anggota_kode'] = '02';
    $wPMR['kabupaten_id'] = 1;
    $rKSR = $this->dm->count_total_by_kabupaten($wKSR);
    // $data['total_ksr'] = $rKSR->num_rows() > 0 ? $rKSR->row()->jml : 0;
    $data['total_ksr'] = 250;
    $data['total_ksr_m'] = 145;
    $data['total_ksr_f'] = 105;

    $wTSR['jenis_anggota_kode'] = '03';
    $wPMR['kabupaten_id'] = 1;
    $rTSR = $this->dm->count_total_by_kabupaten($wTSR);
    // $data['total_tsr'] = $rTSR->num_rows() > 0 ? $rTSR->row()->jml : 0;
    $data['total_tsr'] = 250;
    $data['total_tsr_m'] = 145;
    $data['total_tsr_f'] = 105;

    $wAnggota['jenis_anggota_kode'] = '00';
    $wPMR['kabupaten_id'] = 1;
    $rAnggota = $this->dm->count_total_by_kabupaten($wAnggota);
    // $data['total_anggota'] = $rAnggota->num_rows() > 0 ? $rAnggota->row()->jml : 0;
    $data['total_anggota'] = 18;
    $data['total_anggota_m'] = 16;
    $data['total_anggota_f'] = 2;

    $data['kota'] = 'Jakarta Barat';
    $this->template->load('template/backend', 'dashboard', $data);
  }

  function jakarta_pusat()
  {
    $wPMR['jenis_anggota_kode'] = '01';
    $wPMR['kabupaten_id'] = 2;
    $rPMR = $this->dm->count_total_by_kabupaten($wPMR);
    // $data['total_pmr'] = $rPMR->num_rows() > 0 ? $rPMR->row()->jml : 0;
    $data['total_pmr'] = 392+490+541;
    $data['total_mula'] = 392;
    $data['total_madya'] = 490;
    $data['total_wira'] = 541;

    $wKSR['jenis_anggota_kode'] = '02';
    $wPMR['kabupaten_id'] = 2;
    $rKSR = $this->dm->count_total_by_kabupaten($wKSR);
    // $data['total_ksr'] = $rKSR->num_rows() > 0 ? $rKSR->row()->jml : 0;
    $data['total_ksr'] = 549;
    $data['total_ksr_m'] = 271;
    $data['total_ksr_f'] = 278;

    $wTSR['jenis_anggota_kode'] = '03';
    $wPMR['kabupaten_id'] = 2;
    $rTSR = $this->dm->count_total_by_kabupaten($wTSR);
    // $data['total_tsr'] = $rTSR->num_rows() > 0 ? $rTSR->row()->jml : 0;
    $data['total_tsr'] = 549;
    $data['total_tsr_m'] = 271;
    $data['total_tsr_f'] = 278;

    $wAnggota['jenis_anggota_kode'] = '00';
    $wPMR['kabupaten_id'] = 2;
    $rAnggota = $this->dm->count_total_by_kabupaten($wAnggota);
    // $data['total_anggota'] = $rAnggota->num_rows() > 0 ? $rAnggota->row()->jml : 0;
    $data['total_anggota'] = 23;
    $data['total_anggota_m'] = 17;
    $data['total_anggota_f'] = 6;

    $data['kota'] = 'Jakarta Pusat';
    $this->template->load('template/backend', 'dashboard', $data);
  }

  function jakarta_selatan()
  {
    $wPMR['jenis_anggota_kode'] = '01';
    $wPMR['kabupaten_id'] = 3;
    $rPMR = $this->dm->count_total_by_kabupaten($wPMR);
    // $data['total_pmr'] = $rPMR->num_rows() > 0 ? $rPMR->row()->jml : 0;
    $data['total_pmr'] = 621+2731+1843;
    $data['total_mula'] = 621;
    $data['total_madya'] = 2731;
    $data['total_wira'] = 1843;

    $wKSR['jenis_anggota_kode'] = '02';
    $wPMR['kabupaten_id'] = 3;
    $rKSR = $this->dm->count_total_by_kabupaten($wKSR);
    // $data['total_ksr'] = $rKSR->num_rows() > 0 ? $rKSR->row()->jml : 0;
    $data['total_ksr'] = 1467;
    $data['total_ksr_m'] = 586;
    $data['total_ksr_f'] = 881;

    $wTSR['jenis_anggota_kode'] = '03';
    $wPMR['kabupaten_id'] = 3;
    $rTSR = $this->dm->count_total_by_kabupaten($wTSR);
    // $data['total_tsr'] = $rTSR->num_rows() > 0 ? $rTSR->row()->jml : 0;
    $data['total_tsr'] = 1467;
    $data['total_tsr_m'] = 586;
    $data['total_tsr_f'] = 881;

    $wAnggota['jenis_anggota_kode'] = '00';
    $wPMR['kabupaten_id'] = 3;
    $rAnggota = $this->dm->count_total_by_kabupaten($wAnggota);
    // $data['total_anggota'] = $rAnggota->num_rows() > 0 ? $rAnggota->row()->jml : 0;
    $data['total_anggota'] = 19;
    $data['total_anggota_m'] = 18;
    $data['total_anggota_f'] = 1;

    $data['kota'] = 'Jakarta Selatan';
    $this->template->load('template/backend', 'dashboard', $data);
  }

  function jakarta_timur()
  {
    $wPMR['jenis_anggota_kode'] = '01';
    $wPMR['kabupaten_id'] = 4;
    $rPMR = $this->dm->count_total_by_kabupaten($wPMR);
    // $data['total_pmr'] = $rPMR->num_rows() > 0 ? $rPMR->row()->jml : 0;
    $data['total_pmr'] = 317+864+589;
    $data['total_mula'] = 317;
    $data['total_madya'] = 864;
    $data['total_wira'] = 589;

    $wKSR['jenis_anggota_kode'] = '02';
    $wPMR['kabupaten_id'] = 4;
    $rKSR = $this->dm->count_total_by_kabupaten($wKSR);
    // $data['total_ksr'] = $rKSR->num_rows() > 0 ? $rKSR->row()->jml : 0;
    $data['total_ksr'] = 280;
    $data['total_ksr_m'] = 80;
    $data['total_ksr_f'] = 200;

    $wTSR['jenis_anggota_kode'] = '03';
    $wPMR['kabupaten_id'] = 4;
    $rTSR = $this->dm->count_total_by_kabupaten($wTSR);
    // $data['total_tsr'] = $rTSR->num_rows() > 0 ? $rTSR->row()->jml : 0;
    $data['total_tsr'] = 280;
    $data['total_tsr_m'] = 80;
    $data['total_tsr_f'] = 200;

    $wAnggota['jenis_anggota_kode'] = '00';
    $wPMR['kabupaten_id'] = 4;
    $rAnggota = $this->dm->count_total_by_kabupaten($wAnggota);
    // $data['total_anggota'] = $rAnggota->num_rows() > 0 ? $rAnggota->row()->jml : 0;
    $data['total_anggota'] = 21;
    $data['total_anggota_m'] = 11;
    $data['total_anggota_f'] = 10;

    $data['kota'] = 'Jakarta Timur';
    $this->template->load('template/backend', 'dashboard', $data);
  }

  function jakarta_utara()
  {
    $wPMR['jenis_anggota_kode'] = '01';
    $wPMR['kabupaten_id'] = 5;
    $rPMR = $this->dm->count_total_by_kabupaten($wPMR);
    $data['total_pmr'] = 134+681+336;
    $data['total_mula'] = 134;
    $data['total_madya'] = 681;
    $data['total_wira'] = 336;

    $wKSR['jenis_anggota_kode'] = '02';
    $wPMR['kabupaten_id'] = 5;
    $rKSR = $this->dm->count_total_by_kabupaten($wKSR);
    // $data['total_ksr'] = $rKSR->num_rows() > 0 ? $rKSR->row()->jml : 0;
    $data['total_ksr'] = 78;
    $data['total_ksr_m'] = 43;
    $data['total_ksr_f'] = 35;

    $wTSR['jenis_anggota_kode'] = '03';
    $wPMR['kabupaten_id'] = 5;
    $rTSR = $this->dm->count_total_by_kabupaten($wTSR);
    // $data['total_tsr'] = $rTSR->num_rows() > 0 ? $rTSR->row()->jml : 0;
    $data['total_tsr'] = 78;
    $data['total_tsr_m'] = 43;
    $data['total_tsr_f'] = 35;

    $wAnggota['jenis_anggota_kode'] = '00';
    $wPMR['kabupaten_id'] = 5;
    $rAnggota = $this->dm->count_total_by_kabupaten($wAnggota);
    // $data['total_anggota'] = $rAnggota->num_rows() > 0 ? $rAnggota->row()->jml : 0;
    $data['total_anggota'] = 70;
    $data['total_anggota_m'] = 34;
    $data['total_anggota_f'] = 36;

    $data['kota'] = 'Jakarta Utara';
    $this->template->load('template/backend', 'dashboard', $data);
  }

  function kep_seribu()
  {
    $wPMR['jenis_anggota_kode'] = '01';
    $wPMR['kabupaten_id'] = 6;
    $rPMR = $this->dm->count_total_by_kabupaten($wPMR);
    // $data['total_pmr'] = $rPMR->num_rows() > 0 ? $rPMR->row()->jml : 0;
    $data['total_pmr'] = 140+152+65;
    $data['total_mula'] = 140;
    $data['total_madya'] = 152;
    $data['total_wira'] = 65;

    $wKSR['jenis_anggota_kode'] = '02';
    $wPMR['kabupaten_id'] = 6;
    $rKSR = $this->dm->count_total_by_kabupaten($wKSR);
    // $data['total_ksr'] = $rKSR->num_rows() > 0 ? $rKSR->row()->jml : 0;
    $data['total_ksr'] = 130;
    $data['total_ksr_m'] = 96;
    $data['total_ksr_f'] = 34;

    $wTSR['jenis_anggota_kode'] = '03';
    $wPMR['kabupaten_id'] = 6;
    $rTSR = $this->dm->count_total_by_kabupaten($wTSR);
    // $data['total_tsr'] = $rTSR->num_rows() > 0 ? $rTSR->row()->jml : 0;
    $data['total_tsr'] = 130;
    $data['total_tsr_m'] = 96;
    $data['total_tsr_f'] = 34;

    $wAnggota['jenis_anggota_kode'] = '00';
    $wPMR['kabupaten_id'] = 6;
    $rAnggota = $this->dm->count_total_by_kabupaten($wAnggota);
    // $data['total_anggota'] = $rAnggota->num_rows() > 0 ? $rAnggota->row()->jml : 0;
    $data['total_anggota'] = 9;
    $data['total_anggota_m'] = 9;
    $data['total_anggota_f'] = 0;

    $data['kota'] = 'Kepulauan Seribu';
    $this->template->load('template/backend', 'dashboard', $data);
  }

  public function detail($id)
  {
    $where['id'] = $id;
    $row = $this->vupm->get_data($where)->row();
    $data['row'] = $row;

    $wPelatihan['user_profile_id'] = $id;
    $resPelatihan = $this->vuplm->get_data($wPelatihan)->result();
    $data['resPelatihan'] = $resPelatihan;

    $this->template->load('template/backend', 'detail', $data);
  }

  public function dataGrid()
  {
    $list = $this->vupm->get_datatables($this->input->post());
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $no++;
      $row = array();
      $row[] = $rw->name;
      $row[] = $rw->email;
      $row[] = $rw->gender;
      $row[] = $rw->created_at;
      $row[] = $rw->updated_at;
      $row[] = "<div class=\"text-center\"> <a href=\"".site_url("Dashboard/detail/".$rw->id)."\" class=\"btn btn-primary btn-flat btn-xs\">Detail</a></div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->vupm->count_all(),
      "recordsFiltered" => $this->vupm->count_filtered($this->input->post()),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }
}
