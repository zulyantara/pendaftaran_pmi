<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Unit extends CI_Controller
{
  var $modul = 'Unit';

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Unit_model', 'unit');
    $this->load->model('V_unit_model', 'vUnit');
    $this->load->model('kabupaten_model', 'kabupaten');
    $this->load->model('jenis_anggota_model', 'jenisAnggota');
    $this->load->model('sub_jenis_anggota_model', 'subJenisAnggota');
  }

  function index()
  {
    $rwKabupaten = $this->kabupaten->get_data(NULL);
    $data['rwKabupaten'] = $rwKabupaten->result();

    $rwJenisAnggota = $this->jenisAnggota->get_data(NULL);
    $data['rwJenisAnggota'] = $rwJenisAnggota->result();

    $rwSubJenisAnggota = $this->subJenisAnggota->get_data(NULL);
    $data['rwSubJenisAnggota'] = $rwSubJenisAnggota->result();

    $data["modul"] = $this->modul;
		$data["bt"]= "List ".humanize($this->modul); /* Box Title */

    $this->template->load("template/backend", strtolower($this->modul)."/index", $data);
  }

  public function show($id = NULL)
  {
    $rwKabupaten = $this->kabupaten->get_data(NULL);
    $data['rwKabupaten'] = $rwKabupaten->result();

    $rwJenisAnggota = $this->jenisAnggota->get_data(NULL);
    $data['rwJenisAnggota'] = $rwJenisAnggota->result();

    if (is_null($id)) {
      $data["modul"] = $this->modul;
      $data["bt"]= "List ".humanize($this->modul); /* Box Title */

      $this->template->load("template/backend", strtolower($this->modul)."/form", $data);
    }
    else {
      $params['where']['id'] = $id;
      $row = $this->vUnit->get_data($params)->row();
      $data['row'] = $row;

      $pSubJenisAnggota['where']['jenis_anggota_id'] = $row->jenis_anggota_id;
      $rwSubJenisAnggota = $this->subJenisAnggota->get_data($pSubJenisAnggota);
      $data['rwSubJenisAnggota'] = $rwSubJenisAnggota->result();

      $data["modul"] = $this->modul;
      $data["bt"]= "List ".humanize($this->modul); /* Box Title */

      $this->template->load("template/backend", strtolower($this->modul)."/form", $data);
    }
  }

  public function submit()
  {
    $btn_submit = $this->input->post('submit');
    $id = $this->input->post('id');
    $unit = $this->input->post('unit');
    $kabupaten = $this->input->post('kabupaten');
    $sub_jenis_anggota = $this->input->post('sub_jenis_anggota');

    if ($btn_submit === 'save') {
      $params['where']['unit'] = $unit;
      $params['where']['kabupaten_id'] = $kabupaten;
      $params['where']['sub_jenis_anggota_id'] = $sub_jenis_anggota;
      $rowCheck = $this->unit->get_data($params);

      if ($rowCheck->num_rows() > 0) {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($unit)." sudah ada";
        $this->session->set_flashdata($flashdata);

        redirect($this->modul);
      }

      $data['unit'] = trim(strtoupper($unit));
      $data['kabupaten_id'] = trim($kabupaten);
      $data['sub_jenis_anggota_id'] = trim($sub_jenis_anggota);
      $data['created_at'] = date('Y-m-d');

      $simpan = $this->unit->create($data);

      if ($simpan == 1) {
        $flashdata["alert_class"] = "success";
        $flashdata["alert_ket"] = "Data ".strtoupper($unit)." berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($unit)." tidak berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }

      redirect($this->modul);
    }
    elseif ($btn_submit === 'update') {
      $params['where']['id'] = $id;
      $params['data']['unit'] = trim(strtoupper($unit));
      $params['data']['kabupaten_id'] = trim(strtoupper($kabupaten));
      $params['data']['sub_jenis_anggota_id'] = trim(strtoupper($sub_jenis_anggota));
      $params['data']['updated_at'] = date('Y-m-d');

      $simpan = $this->unit->update($params);

      if ($simpan == 1) {
        $flashdata["alert_class"] = "success";
        $flashdata["alert_ket"] = "Data ".strtoupper($unit)." berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($unit)." tidak berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }

      redirect($this->modul);
    }
    else {
      redirect($this->modul);
    }
  }

  public function destroy()
  {
    $id = $this->input->post('id');
    $params['where']['id'] = $id;
    $this->unit->delete($params);
    echo 'Data berhasil dihapus';
  }

  public function data_grid()
  {
    $list = $this->vUnit->get_datatables($this->input->post());
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $btn_edit = "<a href=\"".site_url($this->modul."/show/".$rw->id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>";

      $btn_delete = "<button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->id."','".$rw->unit."');\">Delete</button>";

      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = strtoupper($rw->unit);
      $row[] = strtoupper($rw->kabupaten);
      $row[] = strtoupper($rw->jenis_anggota);
      $row[] = strtoupper($rw->sub_jenis_anggota);
      $row[] = "<div class=\"text-center\">$btn_edit $btn_delete</div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->vUnit->count_all(),
      "recordsFiltered" => $this->vUnit->count_filtered($this->input->post()),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }

  public function ajax_by_kabupaten_id()
  {
    $params['where']['kabupaten_id'] = $this->input->post('kabupaten_id');
    $this->input->post('sub_jenis_anggota_id') != '' ? $params['where']['sub_jenis_anggota_id'] = $this->input->post('sub_jenis_anggota_id') : '';
    $resUnit = $this->unit->get_data($params);

    if ($resUnit->num_rows() > 0)
    {
      echo json_encode($resUnit->result());
    }
    else
    {
      echo json_last_error_msg();
    }
  }
}
