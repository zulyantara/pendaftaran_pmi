<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Sub_jenis_anggota extends CI_Controller
{
  var $modul = 'Sub_jenis_anggota';

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Jenis_anggota_model', 'jenisAnggota');
    $this->load->model('Sub_jenis_anggota_model', 'subJenisAnggota');
    $this->load->model('V_sub_jenis_anggota_model', 'vSubJenisAnggota');
  }

  function index()
  {
    $rwJenisAnggota = $this->jenisAnggota->get_data(NULL);
    $data['rwJenisAnggota'] = $rwJenisAnggota->result();
    $data["modul"] = $this->modul;
		$data["bt"]= "List ".humanize($this->modul); /* Box Title */

    $this->template->load("template/backend", strtolower($this->modul)."/index", $data);
  }

  public function show($id = NULL)
  {
    if (is_null($id)) {
      $rwJenisAnggota = $this->jenisAnggota->get_data(NULL);
      $data['rwJenisAnggota'] = $rwJenisAnggota->result();
      $data["modul"] = $this->modul;
      $data["bt"]= "List ".humanize($this->modul); /* Box Title */
  
      $this->template->load("template/backend", strtolower($this->modul)."/form", $data);
    }
    else {
      $params['where']['id'] = $id;
      $row = $this->subJenisAnggota->get_data($params);
      $data['row'] = $row->row();

      $rwJenisAnggota = $this->jenisAnggota->get_data(NULL);
      $data['rwJenisAnggota'] = $rwJenisAnggota->result();
      $data["modul"] = $this->modul;
      $data["bt"]= "List ".humanize($this->modul); /* Box Title */
  
      $this->template->load("template/backend", strtolower($this->modul)."/form", $data);
    }
  }

  public function submit()
  {
    $btn_submit = $this->input->post('submit');
    $id = $this->input->post('id');
    $sub_jenis_anggota = $this->input->post('sub_jenis_anggota');
    $jenis_anggota = $this->input->post('jenis_anggota');

    if ($btn_submit === 'save') {
      $params['where']['sub_jenis_anggota'] = $sub_jenis_anggota;
      $rowCheck = $this->subJenisAnggota->get_data($params);

      if ($rowCheck->num_rows() > 0) {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($sub_jenis_anggota)." sudah ada";
        $this->session->set_flashdata($flashdata);
        
        redirect($this->modul);
      }

      $paramsMax['select_max'] = 'kode';
      $rowMax = $this->subJenisAnggota->get_data($paramsMax);
      
      $newKode = $rowMax->row()->kode;
      $kode = str_pad($newKode + 1, 2, '0', STR_PAD_LEFT);

      $data['kode'] = $kode;
      $data['sub_jenis_anggota'] = trim(strtoupper($sub_jenis_anggota));
      $data['jenis_anggota_id'] = trim($jenis_anggota);
      $data['created_at'] = date('Y-m-d');

      $simpan = $this->subJenisAnggota->create($data);

      if ($simpan == 1) {
        $flashdata["alert_class"] = "success";
        $flashdata["alert_ket"] = "Data ".strtoupper($sub_jenis_anggota)." berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($sub_jenis_anggota)." tidak berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      
      redirect($this->modul);
    }
    elseif ($btn_submit === 'update') {
      $params['where']['id'] = $id;
      $params['data']['sub_jenis_anggota'] = trim(strtoupper($sub_jenis_anggota));
      $params['data']['jenis_anggota_id'] = trim($jenis_anggota);
      $params['data']['updated_at'] = date('Y-m-d');

      $simpan = $this->subJenisAnggota->update($params);

      if ($simpan == 1) {
        $flashdata["alert_class"] = "success";
        $flashdata["alert_ket"] = "Data ".strtoupper($sub_jenis_anggota)." berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      else {
        $flashdata["alert_class"] = "warning";
        $flashdata["alert_ket"] = "Data ".strtoupper($sub_jenis_anggota)." tidak berhasil disimpan";
        $this->session->set_flashdata($flashdata);
      }
      
      redirect($this->modul);
    }
    else {
      redirect($this->modul);
    }
  }

  public function destroy()
  {
    $id = $this->input->post('id');
    $params['where']['id'] = $id;
    $this->subJenisAnggota->delete($params);
    echo 'Data berhasil dihapus';
  }

  public function data_grid()
  {
    $list = $this->vSubJenisAnggota->get_datatables($this->input->post());
    $data = array();
    $no = $this->input->post('start');
    foreach ($list as $rw) {
      $btn_edit = "<a href=\"".site_url($this->modul."/show/".$rw->id)."\" class=\"btn btn-primary btn-flat btn-xs\">Edit</a>";

      $btn_delete = "<button type=\"button\" title=\"Hapus Data\" class=\"btn btn-primary btn-flat btn-xs\" onClick=\"deleteItem('".$rw->id."','".$rw->sub_jenis_anggota."');\">Delete</button>";

      $no++;
      $row = array();
      $row[] = '<div class="text-center">'.$no.'</div>';
      $row[] = strtoupper($rw->kode);
      $row[] = strtoupper($rw->sub_jenis_anggota);
      $row[] = strtoupper($rw->jenis_anggota);
      $row[] = "<div class=\"text-center\">$btn_edit $btn_delete</div>";

      $data[] = $row;
    }

    $output = array(
      "draw" => $this->input->post('draw'),
      "recordsTotal" => $this->vSubJenisAnggota->count_all(),
      "recordsFiltered" => $this->vSubJenisAnggota->count_filtered($this->input->post()),
      "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }

  public function ajax_by_jenis_anggota_id()
  {
    $jenis_anggota_id = $this->input->post('jenis_anggota_id');
    $params['where']['jenis_anggota_id'] = $jenis_anggota_id;
    $resSJenisAnggota = $this->subJenisAnggota->get_data($params);

    if ($resSJenisAnggota->num_rows() > 0)
    {
      echo json_encode($resSJenisAnggota->result());
    }
    else
    {
      echo json_last_error_msg();
    }
  }
}
