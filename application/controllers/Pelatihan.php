<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Pelatihan extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Pelatihan_model', 'pm');
  }

  public function ajax_by_jenis_anggota_id()
  {
    $where['jenis_anggota_id'] = $this->input->post('jenis_anggota_id');
    $resPelatihan = $this->pm->get_data($where);

    if ($resPelatihan->num_rows() > 0)
    {
      foreach ($resPelatihan->result() as $val)
      {
        ?>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <!-- <label for="judul">Pelatihan</label> -->
              <input type="checkbox" name="pelatihan[]" value="<?php echo $val->id; ?>"> <?php echo $val->pelatihan; ?>
            </div>
          </div>
          <!-- <div class="col-md-6">
            <div class="form-group">
              <label for="pelatihan">File input</label>
              <input type="file" name="file_pelatihan[]">
            </div>
          </div> -->
        </div>
        <!-- <hr> -->
        <?php
      }
    }
    else
    {
      echo json_last_error_msg();
    }
  }
}
