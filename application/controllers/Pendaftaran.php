<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Zulyantara <zulyantara@gmail.com>
 */
class Pendaftaran extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('User_profile_model', 'upm');
  }

  function index()
  {
    // echo "<pre>";var_dump($this->input->post());echo "</pre>";exit;
    $btn_register = $this->input->post('register');

    if ($btn_register === 'register')
    {
      $save = $this->upm->store($this->input->post());
      if ($save === 'success')
      {
        $this->session->set_flashdata('class', 'success');
        $this->session->set_flashdata('msg', 'Pendaftaran berhasil');
        redirect('Home');
      }
    }
    else
    {
      redirect('Home');
    }
  }

}
